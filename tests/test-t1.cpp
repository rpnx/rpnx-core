//
// Created by rnicholl on 7/24/2022.
//

#include "rpnx/experimental/event_callback_scheduler.hpp"
#include <catch2/catch_test_macros.hpp>
#include <functional>

TEST_CASE("bistate", "owo")
{

    std::allocator< void > alloc;

    std::set< int > my_Set(alloc);
    rpnx::experimental::bistate_source src;

    my_Set.insert(1);

    my_Set.clear();

    src.reset();

    int i = 0;

    std::optional< rpnx::experimental::bistate_immediate_caller< std::function< void(bool) > > > caller;
    caller.emplace(src,
                   [&i](bool b)
                   {
                       if (b == true)
                       {
                           i = 1;
                       }
                       if (b == false)
                       {
                           i = 2;
                       }
                   });

    REQUIRE(i == 0);

    src.signal();

    REQUIRE(i == 1);

    src.reset();

    REQUIRE(i == 2);

    caller = std::nullopt;

    src.signal();
    REQUIRE(i == 2);
    src.reset();
    REQUIRE(i == 2);
}