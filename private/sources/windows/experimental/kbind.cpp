//
// Created by rnicholl on 6/15/2021.
//
#include <winsock2.h>
#include "rpnx/kbind.hpp"


#include <iostream>

rpnx::kbind rpnx::g_kbind{};

rpnx::kbind::kbind()
{
    std::cerr << "Initializing kbind" << std::endl;

    WSAData woo{};
    [[maybe_unused]] auto result = WSAStartup(MAKEWORD(2, 2), &woo);
    // TODO check result
    // this pretty much never returns an error though


    SOCKET socket = INVALID_SOCKET;

    socket = WSASocketW(AF_INET6, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);

    GUID GuidAcceptEx = WSAID_ACCEPTEX;

    DWORD v_bytes{};
    [[maybe_unused]] int iResult = WSAIoctl(socket, SIO_GET_EXTENSION_FUNCTION_POINTER,
                                            &GuidAcceptEx, sizeof (GuidAcceptEx),
                                            &winnt_AcceptEx, sizeof (winnt_AcceptEx),
                                            &v_bytes, nullptr, nullptr);

    //closesocket(socket);
    if (iResult != 0)

    {
        [[maybe_unused]] auto err = WSAGetLastError();
        // TODO better error
        throw std::invalid_argument("SHUTDOWN");
    }

}