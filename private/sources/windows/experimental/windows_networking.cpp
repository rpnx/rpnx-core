// Above is just to help the IDEs find the right place to stick functions...
// we should not be compiling this file unless we are on Windows...
#include "rpnx/experimental/windows_networking.hpp"
#include "rpnx/experimental/result.hpp"
// This is a bug
#undef max
#undef min

#include <utility>
#include <optional>
#include <syncstream>
#include <memory>
#include <optional>
#include <rpnx/experimental/networking_types_async_service.hpp>

#include "rpnx/experimental/network_native_socket_types.hpp"



static inline rpnx::experimental::win32_async_service* impl(void* ptr)
{
    return reinterpret_cast< rpnx::experimental::win32_async_service* >(ptr);
}

rpnx::experimental::async_service::async_service() : m_pimpl(nullptr)
{
    m_pimpl = reinterpret_cast< void* >(new win32_async_service());
}

rpnx::experimental::async_service::~async_service()
{
    delete reinterpret_cast< win32_async_service* >(m_pimpl);
}





template < typename Spec >
void * rpnx::experimental::async_service::bind_autoaccept_spec( typename rpnx::experimental::net_tcp_implementation<Spec>::acceptor_ref ref, rpnx::experimental::actions_type< typename net_tcp_implementation<Spec>::connection > actions)
{
    return reinterpret_cast< win32_async_service* >(m_pimpl)->bind_autoaccept_spec<Spec>((HANDLE) ref.native(), std::move(actions));
}

template < typename Spec >
void * rpnx::experimental::async_service::bind_auto_receive_unlimited_spec(typename net_tcp_implementation< Spec >::connection_ref ref, actions_type< transfer_block > actions)
{
    return reinterpret_cast< win32_async_service* >(m_pimpl)->bind_auto_receive_unlimited_spec<Spec>((HANDLE) ref.native(), std::move(actions));
}


template < typename Spec >
void * rpnx::experimental::async_service::bind_auto_ordered_send_unlimited_spec(typename net_tcp_implementation< Spec >::connection_ref ref, actions_type< std::size_t > actions)
{
    return reinterpret_cast< win32_async_service* >(m_pimpl)->bind_auto_ordered_sender_unlimited_spec<Spec>((HANDLE) ref.native(), std::move(actions));
}



template < typename Spec >
void  rpnx::experimental::async_service::submit_ordered_send_unlimited(void* binding, transfer_block data)
{
    auto implementation = reinterpret_cast< win32_async_service* >(m_pimpl);
    auto ptr = reinterpret_cast<win32_auto_io_binding<async_auto_ordered_send_unlimited_iospec<Spec>> *>(binding);
    bool was_empty = false;
    {
        std::unique_lock lock(ptr->m_io_spec.m_transfer_mtx);
        if (ptr->m_io_spec.m_transfer_queue.empty()) was_empty = true;
        ptr->m_io_spec.m_transfer_queue.insert(ptr->m_io_spec.m_transfer_queue.end(), data.begin(), data.end());
    }
    if (was_empty) implementation->run_sequenced_async([implementation, h = ptr->m_h]{
                                            implementation->wakeup(h);
                                            });
}

// Because we don't implement the following functions in the header files, we need to explicitly instantiate these functions in the .cpp file, because consumers of the library wont be able to compile them themselves.
template void  rpnx::experimental::async_service::submit_ordered_send_unlimited<rpnx::experimental::async_ip4_tcp_specs>(void* binding, transfer_block data);
template void  rpnx::experimental::async_service::submit_ordered_send_unlimited<rpnx::experimental::async_ip6_tcp_specs>(void* binding, transfer_block data);

// Because we don't implement the following functions in the header files, we need to explicitly instantiate these functions in the .cpp file, because consumers of the library wont be able to compile them themselves.
template void * rpnx::experimental::async_service::bind_auto_receive_unlimited_spec<rpnx::experimental::async_ip4_tcp_specs>(typename net_tcp_implementation<rpnx::experimental::async_ip4_tcp_specs >::connection_ref ref, actions_type< transfer_block > actions);



// Because we don't implement the following functions in the header files, we need to explicitly instantiate these functions in the .cpp file, because consumers of the library wont be able to compile them themselves.
template void * rpnx::experimental::async_service::bind_auto_receive_unlimited_spec<rpnx::experimental::async_ip4_tcp_specs>(typename net_tcp_implementation<rpnx::experimental::async_ip4_tcp_specs >::connection_ref ref, actions_type< transfer_block > actions);

// Because we don't implement the following functions in the header files, we need to explicitly instantiate these functions in the .cpp file, because consumers of the library wont be able to compile them themselves.
template void * rpnx::experimental::async_service::bind_auto_receive_unlimited_spec<rpnx::experimental::async_ip6_tcp_specs>(typename net_tcp_implementation<rpnx::experimental::async_ip6_tcp_specs >::connection_ref ref, actions_type< transfer_block > actions);

template void * rpnx::experimental::async_service::bind_auto_ordered_send_unlimited_spec<rpnx::experimental::async_ip4_tcp_specs>(typename net_tcp_implementation<rpnx::experimental::async_ip4_tcp_specs >::connection_ref ref, actions_type< std::size_t > actions);

// Because we don't implement the following functions in the header files, we need to explicitly instantiate these functions in the .cpp file, because consumers of the library wont be able to compile them themselves.
template void * rpnx::experimental::async_service::bind_auto_receive_unlimited_spec<rpnx::experimental::async_ip6_tcp_specs>(typename net_tcp_implementation<rpnx::experimental::async_ip6_tcp_specs >::connection_ref ref, actions_type< transfer_block > actions);



template < typename Spec >
void rpnx::experimental::async_service::unbind_autoaccept_spec(void* ptr)
{
    reinterpret_cast< win32_async_service* >(m_pimpl)->unbind_autoaccept_spec<Spec>(ptr);
}

template < typename Spec >
void rpnx::experimental::async_service::unbind_autoreceive_unlimited_spec(void* ptr)
{
    reinterpret_cast< win32_async_service* >(m_pimpl)->unbind_autoreceive_unlimited_spec<Spec>(ptr);
}



template void rpnx::experimental::async_service::unbind_autoaccept_spec<rpnx::experimental::async_ip6_tcp_specs>(void* ptr);

template void rpnx::experimental::async_service::unbind_autoaccept_spec<rpnx::experimental::async_ip4_tcp_specs>(void* ptr);
template void * rpnx::experimental::async_service::bind_autoaccept_spec<rpnx::experimental::async_ip6_tcp_specs>(typename rpnx::experimental::net_tcp_implementation<rpnx::experimental::async_ip6_tcp_specs>::acceptor_ref, rpnx::experimental::actions_type< typename net_tcp_implementation<rpnx::experimental::async_ip6_tcp_specs>::connection > );

template void *rpnx::experimental::async_service::bind_autoaccept_spec<rpnx::experimental::async_ip4_tcp_specs>(typename rpnx::experimental::net_tcp_implementation<rpnx::experimental::async_ip4_tcp_specs>::acceptor_ref ref, rpnx::experimental::actions_type< typename net_tcp_implementation<rpnx::experimental::async_ip4_tcp_specs>::connection > actions);

template void rpnx::experimental::async_service::unbind_autoreceive_unlimited_spec<rpnx::experimental::async_ip4_tcp_specs>(void* ptr);
template void rpnx::experimental::async_service::unbind_autoreceive_unlimited_spec<rpnx::experimental::async_ip6_tcp_specs>(void* ptr);

template < typename Spec >
void rpnx::experimental::async_service::unbind_auto_ordered_send_unlimited_spec(void* ptr)
{
    reinterpret_cast< win32_async_service* >(m_pimpl)->unbind_auto_ordered_send_unlimited_spec<Spec>(ptr);
}




rpnx::experimental::win32_async_service::win32_async_service()
{
    m_iocp_handle = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, ULONG_PTR(0), 0);
    // TODO: Use proper error handling

    if (m_iocp_handle == INVALID_HANDLE_VALUE) throw std::runtime_error("failed");

    m_stop_signal.store(false, std::memory_order_relaxed);

    m_threads.emplace_back([this]{ run_sequence_thread(); });
    for (int i = 0; i < 4; i++)
    {
        m_threads.emplace_back([this]{ run_thread(); });
    }
}

rpnx::experimental::win32_async_service::~win32_async_service()
{

    assert(m_handlers.size() == 0);
    m_stop_signal.store(true);
    m_sequence_condition.notify_all();
    for (int i = 0; i < m_threads.size(); i++)
    {
        PostQueuedCompletionStatus(m_iocp_handle, 0, 0, nullptr);
    }
    for (std::thread & th : m_threads)
    {
        th.join();
    }
    CloseHandle(m_iocp_handle);
    assert(m_handlers.size() == 0);
}

void rpnx::experimental::win32_async_service::run_thread()
{
   // std::unique_lock m_lock(m_mtx);
    while (true)
    {
        DWORD count{};
        ULONG_PTR ptr{};
        OVERLAPPED* overlapped_ptr = nullptr;
        std::osyncstream(std::cerr) << "Listening for completion status packet" << std::endl;
        auto ok = GetQueuedCompletionStatus(m_iocp_handle, &count, &ptr, &overlapped_ptr, INFINITE);
        std::osyncstream(std::cerr) << "repsonse from GetQueuedCompletionStatus ptr=" << ptr << " count=" << count << std::endl;

        [[maybe_unused]] auto e = GetLastError();
        if (!ok)
        {
            if (ptr == 0)
            {
                std::cerr << "PTR = " << ptr << std::endl;
                std::cerr << "GetQueuedCompletionStatus error " << get_os_network_error_code().message() << std::endl;

                // TODO: Report these errors
                continue; //return;
            }

        }

        if (ptr == 0)
        {
            if (m_stop_signal)
            {
                std::cerr << "STOP" << std::endl;
                return;
            }
        }

        wakeup((HANDLE)ptr);
    }
}
void rpnx::experimental::win32_async_service::wakeup(HANDLE ptr)
{
    std::shared_lock lock(m_handlers_mutex);

    /*
        Another thread could delete this binding before we handle it
        This could happen if two or more completion packets are queued in different threads.
    */
    auto it = m_handlers.find(ptr);
    if (it == m_handlers.end())
    {
        return;
    }

    it->second.iocp_respond(*this, ptr);

    if (it->second.should_delete(*this, ptr))
    {
        /*
            If we should delete the resource, we need to upgrade to an exclusive lock.
            But because we need to release the shared lock temporarily, it's possible a new operation
            could be started or our asynchronous io resource unbound from the async service
            object.
        */
        lock.unlock();

        std::unique_lock lck(m_handlers_mutex);

        /* Case 1: Another thread deleted the resource binding
         */
        it = m_handlers.find(ptr);
        if (it == m_handlers.end())
        {
            return;
        }

        /* Case 2: We are no longer in a "should delete" state
         */
        if (!it->second.should_delete(*this, ptr))
        {
            return;
        }

        /* Case 3: We have exclusive access and the resource binding should be deleted.
         */
        m_handlers.erase(it);
        // TODO: Should we emit an event when a resource binidng is destroyed?
    }
}




void rpnx::experimental::implementation::win32_async_service::run_sequence_thread()
{
    while (true)
    {
        std::unique_lock lock(m_sequence_mutex);

        m_sequence_condition.wait(lock,
        [&] { return m_stop_signal.load(std::memory_order_relaxed) || m_sequence_queue.size() != 0; });

        while (m_sequence_queue.size())
        {
            auto func =  m_sequence_queue.front();
            m_sequence_queue.pop_front();
            lock.unlock();
            func();
            lock.lock();
        }

        if (m_stop_signal.load(std::memory_order_relaxed) && m_sequence_queue.size() == 0)
        {
            m_sequence_condition.notify_all();
            return;
        }
    }
}

template <typename Op, typename ... Ts>
void* rpnx::experimental::implementation::win32_async_service::bind_operation(HANDLE handle, Ts && ... ts)
{
    void * res = nullptr;
    assert(handle != 0 && handle != INVALID_HANDLE_VALUE);
    run_sequenced_sync(
        [handle,  this, &res, &ts...]()
        {
            std::unique_lock ulck(m_handlers_mutex);
            generic_resource_binding_object& obj = m_handlers[handle];
            std::unique_ptr<Op> ptr = std::make_unique<Op> (handle, obj, std::forward<Ts>(ts)...);
            res  = reinterpret_cast<void*>(&*ptr);
            std::unique_ptr<generic_operation_binding_object> binding = std::move(ptr);
            std::unique_lock ulock2(obj.m_mtx);
            if (obj.m_bindings.empty())
            {
                ::CreateIoCompletionPort(handle, m_iocp_handle, (ULONG_PTR) handle, 0);
            }
            obj.m_bindings.emplace_back(std::move(binding));
        });
    run_sequenced_async(
        [h = handle, this]
        {
            wakeup(h);
        });
    return res;
}

void rpnx::experimental::implementation::win32_async_service::bind(HANDLE handle, std::unique_ptr< generic_operation_binding_object > binding)
{
    assert(handle != 0 && handle != INVALID_HANDLE_VALUE);
    run_sequenced_sync(
        [handle, &binding, this]()
        {
            {
                std::unique_lock ulck(m_handlers_mutex);
                generic_resource_binding_object& obj = m_handlers[handle];
                std::unique_lock ulock2(obj.m_mtx);
                obj.m_bindings.emplace_back(std::move(binding));
            }
        });
    run_sequenced_async(
        [h = handle, this]
        {
            wakeup(h);
        });
}


template <typename IOSpec>
void rpnx::experimental::implementation::win32_auto_io_binding<IOSpec>::wakeup(win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h)
{
    assert(h != 0);
    std::osyncstream(std::cerr) << "wakeup " << typeid(IOSpec).name() << " " << h << std::endl;
    std::unique_lock lck(m_mtx);
    while (true)
    {
        iospec_state iostate =  m_io_spec.current_state();
       // std::osyncstream

        switch (iostate)
        {
        case iospec_state::idle_no_work:
        {
            return;
        }
        case iospec_state::idle_ready:
        {
            m_io_spec.start_io(svc, parent, h);
            break;
        }
        case iospec_state::idle_dead:
        {
            return;
        }
        case iospec_state::busy:
        {
            m_io_spec.wakeup(svc, parent, h);
            if (m_io_spec.current_state() == iospec_state::busy)
            {
                return;
            }
            break;
        }
        case iospec_state::result_ready:
        {
            m_actions.m_mainaction(m_io_spec.take_result(svc, parent, h));
            break;
        }
        }

    }
}
/*
void rpnx::experimental::implementation::win32_ip6_tcp_autoaccept_binding::wakeup(win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h)
{
    assert(h != 0);
    std::cout << "WAKEUP " << h << std::endl;
    std::unique_lock lck(m_mtx);
    while (true) 
    {
        if (m_should_terminate && current_state == io_state::idle)
        {
            return;
        }
        else if (current_state == io_state::submitted_pending || current_state == io_state::submitted_cancel_pending) 
        {
            try_complete_io(lck, svc, parent, h);
            if (current_state == io_state::submitted_pending)
                return;
            continue;
        }
        else if (current_state == io_state::idle && ! m_should_terminate)
        {
            start_accept(lck, svc, parent, h);
            continue;
        }
        else
        {
            throw std::runtime_error("Unexpected state");
        }

    }
   
}

*/
template < typename IOSpec >
bool rpnx::experimental::implementation::win32_auto_io_binding< IOSpec >::should_delete(win32_async_service& svc, generic_resource_binding_object&b, HANDLE h)
{
    std::unique_lock lck(m_mtx);
    return m_io_spec.current_state(svc, b, h) == iospec_state::idle_dead;
}

template < typename IOSpec >
void rpnx::experimental::implementation::win32_auto_io_binding< IOSpec >::request_cancel(rpnx::experimental::win32_async_service& svc, HANDLE h, bool& signal, std::mutex& mtx,
                                                                                         std::condition_variable& condvar)
{
    std::unique_lock lck(m_mtx);

    m_after_destroy.push_back(
        [&]
        {
            std::unique_lock lck2(mtx);
            signal = true;
            condvar.notify_all();
        });

    std::osyncstream (std::cerr) << "cancelling " << typeid(IOSpec).name() << " for " << h << std::endl;
    m_io_spec.cancel_io(svc, h);


    svc.run_sequenced_async(
        [h, &svc]
        {
            svc.wakeup(h);
        });


}
template <typename Spec>
void rpnx::experimental::implementation::async_autoaccept_iospec<Spec>::capture_error_result()
{
    std::exception_ptr error_ptr;
    error_ptr = std::make_exception_ptr(network_error("async_autoaccept_iospec::capture_error_result", get_os_network_error_code()));
    m_result.emplace(error_ptr);
}

template <typename Spec>
rpnx::experimental::result<typename rpnx::experimental::implementation::async_autoaccept_iospec<Spec>::result_value_type>  rpnx::experimental::implementation::async_autoaccept_iospec<Spec>::take_result(win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h)
{
    assert(current_state(svc, parent, h) == iospec_state::result_ready);
    assert(m_result.has_value());

    rpnx::experimental::result<result_value_type> res(std::move(*m_result));
    m_result.reset();
    return res;
}

template <typename Spec>
void rpnx::experimental::implementation::async_autoaccept_iospec<Spec>::capture_result_socket()
{
    assert(m_accept_on_socket != INVALID_SOCKET);
    assert(!m_result.has_value());
    m_result.emplace(typename rpnx::experimental::net_tcp_implementation<Spec>::connection(m_accept_on_socket, adopt_resource));
    m_accept_on_socket = INVALID_SOCKET;
}

template <typename Spec>
rpnx::experimental::implementation::iospec_state rpnx::experimental::implementation::async_autoaccept_iospec<Spec>::current_state()
{
    if (m_result.has_value())
    {
        return iospec_state::result_ready;
    }
    else if (!m_io_running && m_should_terminate)
    {
        return iospec_state::idle_dead;
    }
    else if (!m_io_running)
    {
        return iospec_state::idle_ready;
    }
    else
    {
        return iospec_state::busy;
    }
    // This class cannot be in iospec_state::idle_no_work
}

template <typename Spec>
void rpnx::experimental::implementation::async_auto_receive_unlimited_iospec<Spec>::start_io(win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h)
{

    assert(current_state(svc, parent, h) == iospec_state::idle_ready);

    // TODO: convert this is to use net_transfer_buffer s

    assert(!m_result.has_value());
    assert(!m_should_terminate);
    assert(!m_io_running);

    m_overlapped = {};

    DWORD flags =0;
    m_buffer.reset();
   // m_flags = 0;
    std::osyncstream(std::cerr) << " trying to receive on  " << (SOCKET) h <<  std::endl;
    assert(!m_overlapped.has_value());
    m_overlapped.emplace();
    auto result = WSARecv((SOCKET) h, m_buffer.native_ref(), 1, &m_bytes_recvd, &flags, &m_overlapped.value(), nullptr);
    auto erx = WSAGetLastError();
    if (result == SOCKET_ERROR )
    {
        auto err = WSAGetLastError();
        if (err == WSA_IO_PENDING)
        {
            m_io_running = true;
            assert(m_overlapped.has_value());
            assert(current_state() == iospec_state::busy);
            return;
        }
        m_io_running = false;
        m_overlapped.reset();

        capture_error_result(h);
        assert(current_state(svc, parent, h) == iospec_state::result_ready);
        return;
    }
    else if (result != 0)
    {
        std::cerr << "should not be possible" << std::endl;
        std::terminate();
    }
    else if (m_bytes_recvd == 0)
    {
        m_io_running = false;
        m_overlapped.reset();
        m_disconnected = true;
        WSASetLastError(WSAECONNRESET);
        capture_error_result(h);
    }
    else
    {
        std::osyncstream(std::cerr) << "Some bytes received: " << m_bytes_recvd << std::endl;
        m_io_running = false;
        m_overlapped.reset();
        capture_result_data();
       // std::osyncstream (std::cerr) << "Completed already " << h << std::endl;
    }


    return;
    //capture_result_data();

}



template <typename Spec>
void rpnx::experimental::implementation::async_auto_ordered_send_unlimited_iospec<Spec>::start_io(win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h)
{

    assert(current_state(svc, parent, h) == iospec_state::idle_ready);

    // TODO: convert this is to use net_transfer_buffer s

    assert(!m_result.has_value());
    assert(!m_should_terminate);
    assert(!m_io_running);

    m_overlapped = {};

    DWORD flags =0;


    if (m_buffer.size() == 0)
    {
        std::unique_lock lock(m_transfer_mtx);
        m_buffer.reset();
        std::size_t bytes_to_copy = std::min(m_buffer.size(), m_transfer_queue.size());
        std::copy(m_transfer_queue.begin(), m_transfer_queue.begin() + bytes_to_copy, m_buffer.data());
        m_buffer.reslice(0, bytes_to_copy);
        m_transfer_queue.erase(m_transfer_queue.begin(), m_transfer_queue.begin() + bytes_to_copy);
    }

    assert(m_buffer.size() != 0);
    


    std::osyncstream(std::cerr) << " trying to send on  " << (SOCKET) h <<  std::endl;


    assert(!m_overlapped.has_value());
    m_overlapped.emplace();
    auto result = WSASend((SOCKET) h, m_buffer.native_ref(), 1, &m_bytes_sent, flags, &m_overlapped.value(), nullptr);
    auto erx = WSAGetLastError();
    if (result == SOCKET_ERROR )
    {
        auto err = WSAGetLastError();
        if (err == WSA_IO_PENDING)
        {
            m_io_running = true;
            assert(m_overlapped.has_value());
            assert(current_state() == iospec_state::busy);
            return;
        }
        m_io_running = false;
        m_overlapped.reset();

        capture_error_result(h);
        assert(current_state(svc, parent, h) == iospec_state::result_ready);
        return;
    }
    else if (result != 0)
    {
        std::cerr << "should not be possible" << std::endl;
        std::terminate();
    }
    else if (m_bytes_sent == 0)
    {
        m_io_running = false;
        m_overlapped.reset();
        m_disconnected = true;
        WSASetLastError(WSAECONNRESET);
        capture_error_result(h);
    }
    else
    {
        m_io_running = false;
        m_overlapped.reset();
        capture_result_data();
        // std::osyncstream (std::cerr) << "Completed already " << h << std::endl;
    }


    return;
    //capture_result_data();

}


struct on_exit
{
    std::function<void()> f;

    ~on_exit() { f(); }

};


template < typename Spec >
void rpnx::experimental::implementation::async_auto_receive_unlimited_iospec< Spec >::wakeup(rpnx::experimental::win32_async_service& svc, rpnx::experimental::generic_resource_binding_object& parent,
                                                                                             HANDLE h)
{
    assert(m_overlapped.has_value() == m_io_running);
    on_exit f { [&] { assert(m_overlapped.has_value() == m_io_running); } };
    // is the following assumption valid?
    assert(current_state(svc, parent, h) == iospec_state::busy);
    DWORD flags = 0;
    assert(m_overlapped.has_value());
    auto wsaresult = WSAGetOverlappedResult((SOCKET) h, &m_overlapped.value(), &m_bytes_recvd, false, &flags);
    if (wsaresult != TRUE)
    {
        auto error = GetLastError();
        if (error == WSA_IO_INCOMPLETE)
        {
            // Still pending.
            return;
        }
        m_overlapped.reset();
        m_io_running = false;
        if (error == WSA_OPERATION_ABORTED && m_should_terminate)
        {
            return;
        }
        else
        {
            capture_error_result(h);
        }
    }
    else if ( m_bytes_recvd == 0)
    {
        m_disconnected = true;
        m_overlapped.reset();
        m_io_running = false;
        WSASetLastError(WSAECONNRESET);
        capture_error_result(h);
    }

    else
    {
        m_overlapped.reset();
        m_io_running = false;
        capture_result_data();
    }

}

template < typename Spec >
void rpnx::experimental::implementation::async_auto_ordered_send_unlimited_iospec< Spec >::wakeup(rpnx::experimental::win32_async_service& svc, rpnx::experimental::generic_resource_binding_object& parent,
                                                                                             HANDLE h)
{
    assert(m_overlapped.has_value() == m_io_running);
    on_exit f { [&] { assert(m_overlapped.has_value() == m_io_running); } };
    // is the following assumption valid?
    assert(current_state(svc, parent, h) == iospec_state::busy);
    DWORD flags = 0;
    assert(m_overlapped.has_value());
    auto wsaresult = WSAGetOverlappedResult((SOCKET) h, &m_overlapped.value(), &m_bytes_sent, false, &flags);
    if (wsaresult != TRUE)
    {
        auto error = GetLastError();
        if (error == WSA_IO_INCOMPLETE)
        {
            // Still pending.
            return;
        }
        m_overlapped.reset();
        m_io_running = false;
        if (error == WSA_OPERATION_ABORTED && m_should_terminate)
        {
            return;
        }
        else
        {
            capture_error_result(h);
        }
    }
    else if ( m_bytes_sent == 0)
    {
        m_disconnected = true;
        m_overlapped.reset();
        m_io_running = false;
        WSASetLastError(WSAECONNRESET);
        capture_error_result(h);
    }

    else
    {
        m_overlapped.reset();
        m_io_running = false;
        capture_result_data();
    }

}

template <typename Spec>
void rpnx::experimental::implementation::async_auto_receive_unlimited_iospec<Spec>::capture_result_data()
{
    assert(!m_result.has_value());
    assert(!m_io_running);
   if (m_bytes_recvd == 0)
   {
       std::osyncstream (std::cerr) << "Got " << m_bytes_recvd << " bytes" << std::endl;
   }
    m_result.emplace(rpnx::experimental::async_service::transfer_block(m_buffer.data(), m_buffer.data()+m_bytes_recvd));
}

template <typename Spec>
void rpnx::experimental::implementation::async_auto_ordered_send_unlimited_iospec<Spec>::capture_result_data()
{
    assert(!m_result.has_value());
    assert(!m_io_running);
    if (m_bytes_sent == 0)
    {
        std::osyncstream (std::cerr) << "Got " << m_bytes_sent << " bytes" << std::endl;
    }

    std::size_t start1 = m_buffer.get_startpos();
    std::size_t end1 = m_buffer.get_endpos();
    m_buffer.reslice(start1+m_bytes_sent,end1);
    m_result.emplace(m_bytes_sent);
}

template < typename Spec >
rpnx::experimental::iospec_state rpnx::experimental::implementation::async_auto_receive_unlimited_iospec< Spec >::current_state()
{
    assert(m_io_running == m_overlapped.has_value());
    if (m_result.has_value())
    {
        return iospec_state::result_ready;
    }
    else if (!m_io_running && m_should_terminate)
    {
        return iospec_state::idle_dead;
    }
    else if (!m_io_running)
    {
        if (m_disconnected) return iospec_state::idle_no_work;
        else return iospec_state::idle_ready;
    }
    else
    {
        return iospec_state::busy;
    }
}


template < typename Spec >
rpnx::experimental::iospec_state rpnx::experimental::implementation::async_auto_ordered_send_unlimited_iospec< Spec >::current_state()
{
    assert(m_io_running == m_overlapped.has_value());
    if (m_result.has_value())
    {
        return iospec_state::result_ready;
    }
    else if (!m_io_running && m_should_terminate)
    {
        return iospec_state::idle_dead;
    }
    else if (!m_io_running)
    {
        std::unique_lock lock (m_transfer_mtx);

        if (m_disconnected) return iospec_state::idle_no_work;
        if (m_transfer_queue.empty()) return iospec_state::idle_no_work;
        else return iospec_state::idle_ready;
    }
    else
    {
        return iospec_state::busy;
    }
}

template < typename Spec >
rpnx::experimental::result< rpnx::experimental::async_service::transfer_block >
rpnx::experimental::implementation::async_auto_receive_unlimited_iospec< Spec >::take_result(rpnx::experimental::win32_async_service& svc, rpnx::experimental::generic_resource_binding_object& parent,
                                                                                             HANDLE h)
{
    assert(current_state(svc, parent, h) == iospec_state::result_ready);
    assert(m_result.has_value());

    rpnx::experimental::result<result_value_type> res(std::move(*m_result));
    m_result.reset();
    return res;
}


template < typename Spec >
rpnx::experimental::result< std::size_t >
rpnx::experimental::implementation::async_auto_ordered_send_unlimited_iospec< Spec >::take_result(rpnx::experimental::win32_async_service& svc, rpnx::experimental::generic_resource_binding_object& parent,
                                                                                             HANDLE h)
{
    assert(current_state(svc, parent, h) == iospec_state::result_ready);
    assert(m_result.has_value());

    rpnx::experimental::result<result_value_type> res(std::move(*m_result));
    m_result.reset();
    return res;
}

template < typename Spec >
void rpnx::experimental::implementation::async_auto_receive_unlimited_iospec< Spec >::cancel_io(rpnx::experimental::win32_async_service& svc, HANDLE h)
{
    //assert(current_state() == iospec_state::busy);
    assert(!m_should_terminate);
    if (!m_should_terminate)
    {
        m_should_terminate = true;
        if (m_io_running)
        {
            if (::CancelIoEx(h, &m_overlapped.value()) != 0)
            {
                std::osyncstream(std::cerr) << "cancellation error" << std::endl;
            }
            // TODO: Check error?
        }
    }
}

template < typename Spec >
void rpnx::experimental::implementation::async_auto_ordered_send_unlimited_iospec< Spec >::cancel_io(rpnx::experimental::win32_async_service& svc, HANDLE h)
{
    assert(!m_should_terminate);
    if (!m_should_terminate)
    {
        m_should_terminate = true;
        if (m_io_running)
        {
            if (::CancelIoEx(h, &m_overlapped.value()) != 0)
            {
                std::osyncstream(std::cerr) << "cancellation error" << std::endl;
            }
            // TODO: Check error?
        }
    }
}



template < typename Spec >
void rpnx::experimental::implementation::async_auto_receive_unlimited_iospec< Spec >::capture_error_result(HANDLE h)
{
    assert(!m_io_running);
    assert(!m_overlapped.has_value());
    std::exception_ptr error_ptr;
    auto wsae = WSAGetLastError();
    auto er = get_os_network_error_code();
    auto er_ex = network_error("async_autoaccept_iospec::capture_error_result", er);
    std::osyncstream (std::cerr) << "Error: " << h << " " << er_ex.what() << " q=" << wsae << std::endl;

    error_ptr = std::make_exception_ptr(er_ex);
    m_result.emplace(error_ptr);
}


template < typename Spec >
void rpnx::experimental::implementation::async_auto_ordered_send_unlimited_iospec< Spec >::capture_error_result(HANDLE h)
{
    assert(!m_io_running);
    assert(!m_overlapped.has_value());
    std::exception_ptr error_ptr;
    auto wsae = WSAGetLastError();
    auto er = get_os_network_error_code();
    auto er_ex = network_error("async_auto_ordered_send_iospec::capture_error_result", er);
    std::osyncstream (std::cerr) << "Error: " << h << " " << er_ex.what() << " q=" << wsae << std::endl;
    error_ptr = std::make_exception_ptr(er_ex);
    m_result.emplace(error_ptr);
}

template <typename Spec>
void rpnx::experimental::implementation::async_autoaccept_iospec<Spec>::start_io(win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h)
{

    std::cerr << "START Accept" << std::endl;

    assert(current_state(svc, parent, h) == iospec_state::idle_ready);


    assert(m_accept_on_socket == INVALID_SOCKET);
    assert(!m_result.has_value());
    assert(!m_should_terminate);
    assert(!m_io_running);

    m_overlapped = {};
    m_accept_on_socket = WSASocketW(Spec::af, Spec::type, Spec::protocol, nullptr, 0, Spec::dwFlags);
    if (m_accept_on_socket == INVALID_SOCKET)
    {
        capture_error_result();
        assert(current_state(svc, parent, h) == iospec_state::result_ready);
        return;
    }

    DWORD v_bytes_received = 0;
    assert(m_buffer.size() < std::numeric_limits< DWORD >::max());
    assert(h != 0);

    BOOL result = rpnx::g_kbind.winnt_AcceptEx((SOCKET)h,                  // sListenSocket
                                               m_accept_on_socket,         // sAcceptSocket
                                               m_buffer.data(),            // lpOutputBuffer
                                               0,                          // dwReceiveDataLength
                                               (DWORD)m_buffer.size() / 2, // dwLocalAddressLength
                                               (DWORD)m_buffer.size() / 2, // dwRemoteAddressLength
                                               (LPDWORD)&v_bytes_received, // lpdwBytesReceived
                                               &m_overlapped               // lpOverlapped
    );
    static_assert(sizeof(m_buffer) >= (sizeof(typename Spec::native_sockaddr) + 16) * 2);
    if (result == TRUE)
    {
        capture_result_socket();
        m_io_running = false;
        return;
    }

    auto err = WSAGetLastError();
    if (err != ERROR_IO_PENDING)
    {
        capture_error_result();
        closesocket(m_accept_on_socket);
        m_accept_on_socket = INVALID_SOCKET;
        m_io_running = false;
        return;
    }
    else
    {
        m_io_running = true;
    }
}
template <typename Spec>
void rpnx::experimental::implementation::async_autoaccept_iospec<Spec>::cancel_io(win32_async_service& ,  HANDLE h)
{
    assert(current_state() == iospec_state::busy);
    if (m_io_running)
    {
        if (!m_should_terminate)
        {
            m_should_terminate = true;
            ::CancelIoEx(h, &m_overlapped);
            // TODO: Check error?
        }
    }

}

template <typename Spec >
void rpnx::experimental::implementation::async_autoaccept_iospec< Spec >::wakeup(win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h)
{
    assert(current_state(svc, parent, h) == iospec_state::busy);


    DWORD ignore;
    DWORD ignore2;
    [[maybe_unused]] auto wsaresult = WSAGetOverlappedResult((SOCKET) h, &m_overlapped, &ignore, false, &ignore2);
    if (wsaresult != TRUE)
    {
        auto error = GetLastError();
        if (error == WSA_IO_INCOMPLETE)
        {
            // Still pending.
            return;
        }


        m_io_running = false;

        if (error == WSA_OPERATION_ABORTED && m_should_terminate)
        {
            return;
        }
        else
        {
            capture_error_result();
        }

    }

    else
    {
        m_io_running = false;
        capture_result_socket();
    }
}

void rpnx::experimental::implementation::generic_resource_binding_object::iocp_respond(rpnx::experimental::implementation::win32_async_service& svc, HANDLE h)
{

    std::vector < std::unique_ptr< generic_operation_binding_object > > v_objects_to_delete;
    std::unique_lock lck(m_mtx);

    for (std::size_t i = 0; i < m_bindings.size(); i++) 
    {
        generic_operation_binding_object& b = *m_bindings[i];
        b.wakeup(svc, *this, h);
        if (b.should_delete(svc, *this, h))
        {
            std::size_t last_position = m_bindings.size() - 1;
            if (i != last_position)
            {
                std::swap(m_bindings[i], m_bindings[last_position]);
            }
            v_objects_to_delete.push_back(std::move(m_bindings.back()));
            m_bindings.pop_back();
                // TODO: Consider not doing the destroy in a separate loop to avoid an allocation
             
        }

    }
    
    for (auto& x : v_objects_to_delete) 
    {
        for (auto& y : x->m_after_destroy) 
        {
            try
            {
                y();
            }
            catch (...)
            {
                // TODO: Do something here
            }
        }
    }


}

template <typename Op>
void rpnx::experimental::implementation::win32_async_service::unbind_operation(void * ptr)
{
    Op * op = reinterpret_cast<Op*>(ptr);
    HANDLE h = op->m_h;
    bool done = false;
    std::mutex done_mtx;
    std::condition_variable done_condvar;
    op->request_cancel(*this, h, done, done_mtx, done_condvar);
    std::unique_lock lck(done_mtx);
    done_condvar.wait(lck,
                      [&]
                      {
                          return done;
                      });
}



template < typename Spec >
void* rpnx::experimental::implementation::win32_async_service::bind_autoaccept_spec(HANDLE h, rpnx::experimental::actions_type< typename rpnx::experimental::net_tcp_implementation< Spec >::connection > actions)
{
    return bind_operation<win32_auto_io_binding< async_autoaccept_iospec<Spec> > >(h, std::move(actions));
}
template < typename Spec >
void rpnx::experimental::implementation::win32_async_service::unbind_autoaccept_spec(void* ptr)
{
    unbind_operation<win32_auto_io_binding< async_autoaccept_iospec<Spec> >>(ptr);
}

template < typename Spec >
void rpnx::experimental::implementation::win32_async_service::unbind_autoreceive_unlimited_spec(void* ptr)
{
    unbind_operation<win32_auto_io_binding< async_auto_receive_unlimited_iospec<Spec> >>(ptr);
}

template < typename Spec >
void rpnx::experimental::implementation::win32_async_service::unbind_auto_ordered_send_unlimited_spec(void* ptr)
{
    unbind_operation<win32_auto_io_binding< async_auto_ordered_send_unlimited_iospec<Spec> >>(ptr);
}



template < typename Spec >
void* rpnx::experimental::implementation::win32_async_service::bind_auto_receive_unlimited_spec(HANDLE h, rpnx::experimental::actions_type< async_service::transfer_block > actions)
{
    return bind_operation<win32_auto_io_binding< async_auto_receive_unlimited_iospec<Spec> > >(h, std::move(actions));
}

template < typename Spec >
void* rpnx::experimental::implementation::win32_async_service::bind_auto_ordered_sender_unlimited_spec(HANDLE h, rpnx::experimental::actions_type< std::size_t > actions)
{
    return bind_operation<win32_auto_io_binding< async_auto_ordered_send_unlimited_iospec<Spec> > >(h, std::move(actions));
}


bool rpnx::experimental::implementation::generic_resource_binding_object::should_delete(win32_async_service& , HANDLE )
{
    std::shared_lock lck(m_mtx);
    return m_bindings.empty();
}



template void rpnx::experimental::async_service::unbind_auto_ordered_send_unlimited_spec<rpnx::experimental::async_ip4_tcp_specs>(void* ptr);
template void rpnx::experimental::async_service::unbind_auto_ordered_send_unlimited_spec<rpnx::experimental::async_ip6_tcp_specs>(void* ptr);






