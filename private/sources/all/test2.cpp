//#include "rpnx/kbind.hpp"
#include "rpnx/experimental/network.hpp"

#include "rpnx/experimental/thread_pool.hpp"
#include <chrono>
#include <iostream>
#include <optional>
#include <rpnx/experimental/priority_dispatcher.hpp>
#include <thread>
#include <rpnx/experimental/channel.hpp>
#include <functional>
#include <rpnx/experimental/networking_autoserver.hpp>



class server
{
  public:
    class session;
  private:
    rpnx::experimental::auto_channel_processor<rpnx::experimental::async_ip4_tcp_autoacceptor::result_type>
         m_connection_processor;


    void on_connection(rpnx::experimental::async_ip4_tcp_autoacceptor::result_type res)
    {
        std::cout << "net connect, is error? " << (res.is_error() ? "y" : "n") << std::endl;
        // meow
    }


    void tell_closed(session & ses)
    {

    }

  public:

    server()
    : m_connection_processor(
    std::function<void(rpnx::experimental::async_ip4_tcp_autoacceptor::result_type)>(
              [this](rpnx::experimental::async_ip4_tcp_autoacceptor::result_type res)
              {
                return on_connection(std::move(res));
              }
            )
          )//,
        //  m_data_processor( [this](auto res) { return on_data(std::move(res));  } )


    {
    }



    class session
    {
        rpnx::experimental::async_ip4_tcp_connection connection;
        rpnx::experimental::channel<rpnx::experimental::result<rpnx::experimental::async_service::transfer_block> > recvd_data;
        rpnx::experimental::async_ip4_tcp_auto_receiver_unlimited m_recver;
        rpnx::experimental::auto_channel_processor<rpnx::experimental::result<rpnx::experimental::async_service::transfer_block> > m_recv_processor;
        rpnx::experimental::channel<rpnx::experimental::result<std::size_t> > sent_data;
        rpnx::experimental::async_ip4_tcp_auto_ordered_sender_unlimited m_sender;
        rpnx::experimental::channel_registration_binder<rpnx::experimental::result<rpnx::experimental::async_service::transfer_block> > m_bind_recv;
            // rpnx::experimental::async_ip4_tcp_autoreceiver recvr;

        //rpnx::experimental::async_service & svc;

      public:
        session(rpnx::experimental::async_ip4_tcp_connection con,
                rpnx::experimental::async_service & svc,
                server & srv
                )
            : connection(std::move(con)),
              m_recver(connection, svc, rpnx::experimental::channel_submitter(recvd_data)),
              m_recv_processor( [this](rpnx::experimental::result<rpnx::experimental::async_service::transfer_block> res)
                     {
                         return this->receive(std::move(res));
                     }),
              m_sender(connection, svc, rpnx::experimental::channel_submitter(sent_data)),
              m_bind_recv(recvd_data, m_recv_processor)
        {}

        [[maybe_unused]] void receive(rpnx::experimental::result<rpnx::experimental::async_service::transfer_block> in)
        {
            std::osyncstream(std::cerr) << "Got data? " << (in.is_error() ? "no, error instead" : "yes") << std::endl;

            if (!in.is_error())
            {
                std::osyncstream(std::cerr) << "How many bytes of data: " << in.get().size() << std::endl;
            }

            try
            {
                std::string hello = "hello";
                m_sender.send_async((std::byte*)hello.data(), (std::byte*)hello.data() + hello.size());

                std::string foo;

                foo.assign((char*) in.get().data(), (char*) in.get().data() + in.get().size());

                std::osyncstream(std::cerr) << "Data:" << std::endl << foo << std::endl;
            }
            catch (std::exception & ex )
            {
                std::osyncstream (std::cerr) << "Error: " << ex.what() << std::endl;
            }



        }
    };


   // server()
    //: m_connection_processor() {}
};



int main()
{
    rpnx::experimental::network_enabled_context context;
#if _WIN32
    assert(rpnx::g_kbind.winnt_AcceptEx != nullptr);
#endif
    try
    {
        auto on_transfer_error = [&](std::exception_ptr ptr)
        {
            try
            {
                std::cerr << "MainAction failed" << std::endl;
                std::terminate();
            }
            catch (...)
            {

            }
        };
        rpnx::experimental::async_service async_svc;
        std::mutex session_mtx;
        server srv;
        std::map <std::uint64_t, server::session> sessions;
        std::uint64_t session_id = 1;
        std::mutex connections_mtx;
        rpnx::experimental::channel< rpnx::experimental::result<rpnx::experimental::async_ip4_tcp_connection> > connections_channel;

        std::vector<rpnx::experimental::async_ip4_tcp_connection> connections;

        auto get_connection = [&](rpnx::experimental::result<rpnx::experimental::async_ip4_tcp_connection> result)
        {
            std::cout << "Got a connection event" << std::endl;

            try
            {
                rpnx::experimental::async_ip4_tcp_connection con = result.take();
                std::unique_lock lock(session_mtx);
                int id = session_id++;
                sessions.emplace(std::piecewise_construct,
                  std::forward_as_tuple(id),
                  std::forward_as_tuple(std::move(con), async_svc, srv));
            }
            catch (std::exception & ex)
            {
                std::cerr << "Connection FAIL" << ex.what() << std::endl;
            }
        };

        rpnx::experimental::auto_channel_processor<rpnx::experimental::result<rpnx::experimental::async_ip4_tcp_connection>> processor(get_connection);
        rpnx::experimental::channel_registration_binder binding(connections_channel, processor);
        rpnx::experimental::async_ip4_tcp_acceptor acceptor6(rpnx::experimental::ip4_tcp_endpoint{rpnx::experimental::ip4_address(), 8080});
        rpnx::experimental::channel_submitter submitter(connections_channel);
        using namespace rpnx::experimental;
        rpnx::experimental::async_ip4_tcp_autoacceptor auto_acceptor6(acceptor6,
                                                                      async_svc,
                                                                      try_action(channel_submitter(connections_channel),
                                                                                 on_transfer_error)
                                                                      );




        //while (true)
            std::this_thread::sleep_for(std::chrono::seconds(10));
        return 0;
    }
    catch (std::exception const& err)
    {
        std::cerr << err.what() << std::endl;
    }
}