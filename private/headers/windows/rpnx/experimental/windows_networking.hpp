#ifdef _WIN32
// Above is just to help the IDEs find the right place to stick functions...
// we should not be compiling this file unless we are on Windows...
#ifndef RPNXCORE_WINDOWS_NETWORKING_HPP
#define RPNXCORE_WINDOWS_NETWORKING_HPP

#include "rpnx/experimental/net_transfer_buffer.hpp"
#include "rpnx/experimental/network.hpp"

#include <ioapiset.h>
#include <windows.h>

#include <map>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <shared_mutex>
#include <set>
#include <functional>


namespace rpnx
{
    namespace experimental
    {
        inline namespace implementation
        {
            enum class iospec_state
            {
                idle_ready,
                idle_no_work,
                idle_dead,
                busy,
                result_ready
            };

            enum class io_state
            {
                idle,                
                presubmit_ready,
                presubmit_setup_failed,
                submitted_pending,
                submitted_cancel_pending,
                submission_failed,
                completed_success,
                completed_error,
                completed_cancelled
               
            };



            struct win32_async_service;

            struct iocp_handler
            {
                std::shared_mutex m_mtx;
                virtual ~iocp_handler() {};
                [[maybe_unused]] virtual void iocp_respond(win32_async_service& response) = 0;
                virtual bool should_delete() = 0;
            };


            struct generic_operation_binding_object;

            struct generic_resource_binding_object final
               // : public iocp_handler
            {
                std::shared_mutex m_mtx;
                std::vector< std::unique_ptr<generic_operation_binding_object> > m_bindings;
                std::vector< std::function<void()> > m_after_destroy;

              public:
                void iocp_respond(win32_async_service& response, HANDLE h);
                bool should_delete(win32_async_service& response, HANDLE h);
            };

            struct generic_operation_binding_object
            {
                virtual ~generic_operation_binding_object()
                {
                }
                virtual bool should_delete(win32_async_service& response, generic_resource_binding_object& parent, HANDLE h) = 0;
                std::vector< std::function<void()> > m_after_destroy;
                virtual void wakeup(win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h) = 0;
            };


            template <typename IOSpec>
            struct win32_auto_io_binding : public generic_operation_binding_object
            {
                using result_value_type = typename IOSpec::result_value_type;


                win32_auto_io_binding(HANDLE h, generic_resource_binding_object&, actions_type< result_value_type > actions) : m_h(h), m_actions(std::move(actions))
                {
                }

                std::mutex m_mtx;
                HANDLE m_h; // not sure if this field is needed.
                actions_type< result_value_type > m_actions;
                IOSpec m_io_spec;


                bool should_delete(win32_async_service& response, generic_resource_binding_object&, HANDLE h) override;
                void wakeup(win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h) override;
                void request_cancel(win32_async_service& svc,  HANDLE h, bool & signal, std::mutex & mtx, std::condition_variable & condvar);

            };

            template <typename Spec>
            struct async_autoaccept_iospec
            {
                using result_value_type = typename net_tcp_implementation<Spec>::connection;

                std::optional<rpnx::experimental::result<result_value_type>> m_result;
               // async_ip6_tcp_acceptor_ref m_socket;
                OVERLAPPED m_overlapped = {};
                SOCKET m_accept_on_socket = native_invalid_socket_value;
                bool m_should_terminate = false;
                bool m_io_running = false;
                std::array< std::uint8_t, (16+2 * sizeof(typename Spec::native_sockaddr))* 8 > m_buffer = {};

                inline iospec_state current_state( win32_async_service& , generic_resource_binding_object& , HANDLE  ) { return current_state(); }
                iospec_state current_state( );
                void start_io( win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h );
                void wakeup(win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h);
                rpnx::experimental::result<result_value_type> take_result(win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h);
                void cancel_io(win32_async_service& svc, HANDLE h);
                void capture_error_result();
                void capture_result_socket();
            };

            /** Internal class for implementing the autoreceive unlimited operations
             *
             * @tparam Spec The specification used to detemrine the type of socket.
             */
            template <typename Spec>
            struct async_auto_receive_unlimited_iospec
            {
                using result_value_type = async_service::transfer_block;

                std::optional<rpnx::experimental::result<result_value_type>> m_result;
                net_transfer_array<1024> m_buffer;
                std::optional<OVERLAPPED> m_overlapped;
                DWORD m_bytes_recvd;
                DWORD m_flags;
                bool m_disconnected = false;
                bool m_should_terminate = false;
                bool m_io_running = false;
                //std::array< std::uint8_t, (16+2 * sizeof(typename Spec::native_sockaddr))* 8 > m_buffer = {};

                inline iospec_state current_state( win32_async_service& , generic_resource_binding_object& , HANDLE  ) { return current_state(); }
                iospec_state current_state( );
                void start_io( win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h );
                void wakeup(win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h);
                rpnx::experimental::result<result_value_type> take_result(win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h);
                void cancel_io(win32_async_service& svc, HANDLE h);
                void capture_error_result(HANDLE h);
                void capture_result_data();
            };

            /** Internal class for implementing the autoreceive unlimited operations
             *
             * @tparam Spec The specification used to detemrine the type of socket.
             */
            template <typename Spec>
            struct async_auto_ordered_send_unlimited_iospec
            {
                using result_value_type = std::size_t;

                std::optional<rpnx::experimental::result<result_value_type>> m_result;
                net_transfer_array<2000> m_buffer;
                std::optional<OVERLAPPED> m_overlapped;
                DWORD m_bytes_sent;
                DWORD m_flags;
                bool m_disconnected = false;
                bool m_should_terminate = false;
                bool m_io_running = false;

                // This structure is not thread safe, except that data can be added to the transfer queue by another thread using the
                // provided mutex.
                // After doing this, the caller is expected to schedule a wakeup if the queue was empty.
                std::mutex m_transfer_mtx;
                std::deque<std::byte> m_transfer_queue;


                inline async_auto_ordered_send_unlimited_iospec()
                {
                    m_buffer.reslice(0,0);
                }

                inline iospec_state current_state( win32_async_service& , generic_resource_binding_object& , HANDLE  ) { return current_state(); }
                iospec_state current_state( );
                void start_io( win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h );
                void wakeup(win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h);
                rpnx::experimental::result<result_value_type> take_result(win32_async_service& svc, generic_resource_binding_object& parent, HANDLE h);
                void cancel_io(win32_async_service& svc, HANDLE h);
                void capture_error_result(HANDLE h);
                void capture_result_data();
            };


            struct win32_async_service
            {
                friend struct iocp_handler;
                using binding_map = std::map<HANDLE, generic_resource_binding_object >;

              private:

                std::vector<std::thread> m_threads;

                std::mutex m_mtx;
                std::condition_variable m_cond;
                std::atomic<bool> m_stop_signal;
                std::mutex m_sequence_mutex;
                std::condition_variable m_sequence_condition;
                std::shared_mutex m_handlers_mutex;
                std::deque<std::function<void()> > m_sequence_queue;
                
                binding_map m_handlers;
                HANDLE m_iocp_handle;

              public:
                win32_async_service();
                ~win32_async_service();

                void bind(HANDLE, std::unique_ptr< generic_operation_binding_object > binding);

                template <typename Op, typename ... Ts>
                void* bind_operation(HANDLE handle, Ts && ... ts);

                template <typename Op>
                void unbind_operation(void * ptr);

                void wakeup(HANDLE);


                template <typename Spec>
                [[deprecated]] void * bind_autoaccept_spec(HANDLE h, actions_type< typename net_tcp_implementation<Spec>::connection >);

                template <typename Spec>
                void * bind_auto_receive_unlimited_spec(HANDLE h, actions_type< async_service::transfer_block >);

                template <typename Spec>
                void * bind_auto_ordered_sender_unlimited_spec(HANDLE h, actions_type< std::size_t >);



                template <typename Spec>
                void unbind_autoaccept_spec(void* ptr);


                template <typename Spec>
                void unbind_autoreceive_unlimited_spec(void* ptr);

                template <typename Spec>
                void unbind_auto_ordered_send_unlimited_spec(void* ptr);


                void run_sequenced_async(std::function<void()>);
                void run_sequenced_sync(std::function<void()>);

                void run_thread();

                void run_sequence_thread();
            };



        } // namespace implementation

    }     // namespace experimental
} // namespace rpnx



void rpnx::experimental::implementation::win32_async_service::run_sequenced_sync(std::function< void() > thing)
{
    auto task = std::packaged_task(thing);
    auto ftr = task.get_future();
    run_sequenced_async([&]{task();});
    ftr.get();
}

void rpnx::experimental::implementation::win32_async_service::run_sequenced_async(std::function< void() > thing)
{
    std::unique_lock lock(m_sequence_mutex);
    m_sequence_queue.push_back(std::move(thing));
    m_sequence_condition.notify_all();
}
#endif // RPNXCORE_WINDOWS_NETWORKING_HPP
#endif