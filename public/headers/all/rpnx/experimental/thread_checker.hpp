//
// Created by rnicholl on 6/13/2022.
//

#ifndef RPNXCORE_THREAD_CHECKER_HPP
#define RPNXCORE_THREAD_CHECKER_HPP
#include <atomic>
#include <random>
#include <thread>

namespace rpnx::experimental
{

    class thread_checker
    {
        std::atomic<std::size_t> val;

      public:

        std::size_t start()
        {
            thread_local std::random_device r;
            std::size_t n = 0;
            while (n == 0)
            {
                n = r();
            }
            if (val.load(std::memory_order_relaxed) != 0)
            {
                std::terminate();
            }
            val.store(n, std::memory_order_relaxed);

            return n;
        }

        void stop(std::size_t n)
        {
            if (val.load(std::memory_order_relaxed) != n)
            {
                std::terminate();
            }

            val.store(0, std::memory_order_relaxed);

            return;
        }
    };
}

#endif // RPNXCORE_THREAD_CHECKER_HPP
