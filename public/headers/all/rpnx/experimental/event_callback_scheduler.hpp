//
// Created by rnicholl on 7/24/2022.
//

#ifndef RPNXCORE_EVENT_CALLBACK_SCHEDULER_HPP
#define RPNXCORE_EVENT_CALLBACK_SCHEDULER_HPP

#include "rpnx/experimental/event_queue2.hpp"

namespace rpnx::experimental
{

    /** An object which can respond to bistate event transitions by calling a callback in the immediate context of
     * the state transition.
     *
     * @tparam Func A function that accepts bool to be called when the bistate transitions to a new state.
     * @tparam Alloc
     */
    template < typename Func, typename Alloc >
    class basic_bistate_immediate_caller
    {
        Func m_functor;
        detail::raw_bistate_handler m_handler;
        basic_bistate_source< Alloc >& m_source;
      public:
        basic_bistate_immediate_caller(basic_bistate_source< Alloc >& event_source, Func functor)
        : m_functor(functor), m_source(event_source)
        {
            m_handler.m_enable = [](void* context)
            {
                auto self = reinterpret_cast< basic_bistate_immediate_caller* >(context);
                self->m_functor(true);
            };
            m_handler.m_disable = [](void* context)
            {
                auto self = reinterpret_cast< basic_bistate_immediate_caller* >(context);
                self->m_functor(false);
            };
            m_handler.m_context = reinterpret_cast<void*>(this);
            m_source.attach_raw_event_watch(&m_handler);
        }
        ~basic_bistate_immediate_caller()
        {
            m_source.detach_raw_event_watch(&m_handler);
        }

        basic_bistate_immediate_caller(const basic_bistate_immediate_caller&) = delete;
        basic_bistate_immediate_caller& operator=(const basic_bistate_immediate_caller&) = delete;
    };

    template <typename Func>
    using bistate_immediate_caller = basic_bistate_immediate_caller<Func, std::allocator<void*> >;


    /**
     * @brief A scheduler that runs callbacks on a separate thread when the bistate transitions
     * @tparam T
     * @tparam What The type of a function like object that accepts a bool as parameter
     * @tparam ThreadPool An object that accepts a parameterless functor to enqueue
     * @tparam Alloc the allocator to use for any internal data structures.
     */
    template < typename What, typename ThreadPool, typename Alloc >
    class basic_bistate_callback_scheduler
    {
        What wh;
        detail::raw_bistate_handler m_handler;
        ThreadPool& m_thread_pool;
        basic_bistate_source< Alloc >& ev;

      public:
        /**
         * @brief Construct a new basic_bistate_callback_scheduler object
         * @param source The source for events to be scheduled on the thread pool.
         * @param th The thread pool to schedule events on
         * @param wh The functor to be called.
         *
         * When this object is destroyed, the enqueue function cannot be called again,
         * and no race can exist to further calls to enqueue, but this object does not
         * guarantee that calls to the functor have completed before returning.
         *
         */
        basic_bistate_callback_scheduler(basic_bistate_source< Alloc >& source, ThreadPool& th, What wh) : m_thread_pool(th), ev(source)
        {
            m_handler.m_context = reinterpret_cast< void* >(this);
            m_handler.m_enable = [](void* context)
            {
                auto* self = reinterpret_cast< basic_bistate_callback_scheduler* >(context);
                self->m_thread_pool.enqueue(
                    [wh = self->wh]()
                    {
                        wh(true);
                    });
            };
            m_handler.m_disable = [](void* context)
            {
                auto* self = reinterpret_cast< basic_bistate_callback_scheduler* >(context);
                self->m_thread_pool.enqueue(
                    [wh = self->wh]()
                    {
                        wh(false);
                    });
            };
            m_handler.m_detach = [](void* context)
            {
            };
            ev.attach_raw_event_watch(&m_handler);
        }

        ~basic_bistate_callback_scheduler()
        {

            ev.detach_raw_event_watch(&m_handler);
        }
    };

} // namespace rpnx::experimental

#endif // RPNXCORE_EVENT_CALLBACK_SCHEDULER_HPP
