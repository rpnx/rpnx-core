//
// Created by rnicholl on 1/8/2022.
//

#ifndef RPNXCORE_NETWORKING_BASIC_SYNC_API_HPP
#define RPNXCORE_NETWORKING_BASIC_SYNC_API_HPP

#include "rpnx/experimental/networking_basic_sync_api_fwd.hpp"

#include "rpnx/experimental/networking_types_armos.hpp"


namespace rpnx::experimental
{


    template < typename It >
    void net_send(ip4_udp_socket_ref socket, ip4_udp_endpoint const& to, It begin_packet, It end_packet)
    {
#ifdef _WIN32
        sockaddr_in dest = to.native();
        std::vector< char > data;
        for (auto it = begin_packet; it != end_packet; ++it)
        {
            data.emplace_back((char)*it);
        }

        assert((data.size() <= std::numeric_limits<DWORD>::max()));
        auto result = sendto(socket.native(), (char const*)data.data(), static_cast<DWORD>(data.size()), 0, (sockaddr*)&dest, sizeof(dest));
        if (result == SOCKET_ERROR)
        {
            throw network_error("upd_ip4_socket::send()", get_os_network_error_code());
        }
#else
        sockaddr_in dest = to.native();
        std::vector< std::byte > data(begin_packet, end_packet);

        auto result = sendto(socket.native(), (char const*)data.data(), data.size(), 0, (native_sockaddr*)&dest, sizeof(dest));
        if (result == -1)
        {
            throw network_error("upd_ip4_socket::send()", get_os_network_error_code());
        }
        return;
#endif
    }
        template < typename It >
        void net_send(ip4_tcp_connection_ref socket, It begin_data, It end_data)
        {
#ifdef _WIN32

            std::vector< char > data;
            data.assign(begin_data, end_data);

            // TODO: Make this function support more than 2^32 byte transfers

            // I don't know why you'd send that much data in one block today, but
            // future programs running on more powerful computers might consider that normal.

            if (data.size() >= INT_MAX)
                throw std::invalid_argument("send too large");

            size_t st = 0;

            while (st != data.size())
            {
                auto count = send(socket.native(), (const char*)data.data() + st, data.size() - st, 0);

                if (count != SOCKET_ERROR)
                {
                    st += count;
                }
                else
                {
                    throw network_error("rpnx::net_send", get_os_network_error_code());
                }
            }

#else

            std::vector< char > data(begin_data, end_data);

            std::size_t st = 0;

            while (st != data.size())
            {
                auto count = ::send(socket.native(), (const char*)data.data() + st, data.size() - st, 0);

                if (count != -1)
                {
                    st += count;
                }
                else
                {
                    throw network_error("rpnx::net_send", get_os_network_error_code());
                }
            }

#endif
        }
        template < typename It >
        auto net_receive(ip4_udp_socket_ref socket, ip4_udp_endpoint& fr, It begin_packet, It end_packet) -> It
        {
            std::array< std::byte, 256 * 256 > buffer;
            sockaddr_in from;
            static_assert(sizeof(from) < INT_MAX);
#ifdef _WIN32
            int fromlen = sizeof(from);
#else
            socklen_t fromlen = sizeof(from);
#endif
            auto count = recvfrom(socket.native(), reinterpret_cast< char* >(buffer.data()), (int)buffer.size(), 0, reinterpret_cast< sockaddr* >(&from), &fromlen);
            if (count == -1)
            {
                throw network_error("upd_ip4_socket::receive()", get_os_network_error_code());
            }

            It it = begin_packet;
            auto buf_it = buffer.begin();
            auto buf_end = buffer.begin() + count;
            while (it != end_packet && buf_it != buf_end)
            {
                *it = *buf_it;
                ++it;
                ++buf_it;
            }

            if (it == end_packet && buf_it != buf_end)
            {
                throw std::out_of_range("Iterator out of range in rpnx::net_receive");
            }

            fr = ip4_udp_endpoint(from);

            return it;
        }
        template < typename It >
        auto net_receive(ip4_udp_socket_ref socket, ip4_udp_endpoint& fr, It begin_packet) -> It
        {
            std::array< char, 256 * 256 > buffer;
            sockaddr_in from{};
            static_assert(sizeof(from) < INT_MAX);

#ifdef _WIN32
            int from_len = sizeof(from);
#else
            socklen_t from_len = sizeof(from);
#endif

            auto count = recvfrom(socket.native(), reinterpret_cast< char* >(buffer.data()), (int)buffer.size(), 0, reinterpret_cast< sockaddr* >(&from), &from_len);
            if (count == -1)
            {
                throw network_error("upd_ip4_socket::receive()", get_os_network_error_code());
            }

            It it = begin_packet;
            auto buf_it = buffer.begin();
            auto buf_end = buffer.begin() + count;
            while (buf_it != buf_end)
            {
                *it = std::byte(*buf_it);
                ++it;
                ++buf_it;
            }
            fr = ip4_udp_endpoint(from);
            return it;
        }
        inline void net_bind(ip4_udp_socket_ref socket, ip4_udp_endpoint const& bind_addr)
        {
            sockaddr_in socket_addr = bind_addr.native();
            auto result = bind(socket.native(), (const sockaddr*)&socket_addr, sizeof(socket_addr));
            if (result == -1)
            {
                throw network_error("upd_ip4_socket::bind()", get_os_network_error_code());
            }
            return;
        }
        inline ip4_tcp_connection net_accept_connection(ip4_tcp_acceptor_ref socket)
        {
#ifdef _WIN32
            ip4_tcp_connection con = ip4_tcp_connection(accept(socket.native(), nullptr, nullptr));
            if (con.native() == INVALID_SOCKET)
            {
                throw network_error("rpnx::net_accept_connection", get_os_network_error_code());
            }

            return std::move(con);
#else
            ip4_tcp_connection con = ip4_tcp_connection(::accept(socket.native(), nullptr, nullptr));
            if (con.native() == -1)
            {
                throw network_error("rpnx::net_accept_connection", get_os_network_error_code());
            }

            return std::move(con);
#endif
        }
        inline void net_bind(async_ip4_udp_socket_ref socket, ip4_udp_endpoint const& bind_addr)
        {
            sockaddr_in socket_addr = bind_addr.native();
            auto result = bind(socket.native(), (const sockaddr*)&socket_addr, sizeof(socket_addr));
            if (result == -1)
            {
                throw network_error("upd_ip4_socket::bind()", get_os_network_error_code());
            }
            return;
        } // "asynchronously" binds a socket.
        // Note: Synchronous sockets that are bound asynchronously, actually bind synchronously in a different thread.
        inline std::future<void> net_bind_async(ip4_udp_socket_ref socket, ip4_udp_endpoint const& bind_addr)
        {
            return std::async([&socket, bind_addr] {
               sockaddr_in socket_addr = bind_addr.native();
               auto result = bind(socket.native(), (const sockaddr*)&socket_addr, sizeof(socket_addr));
               if (result == -1)
               {
                   throw network_error("upd_ip4_socket::bind()", get_os_network_error_code());
               }
               return;
            });
        }
#if false
        inline ip4_udp_endpoint net_endpoint(ip4_udp_socket& socket)
        {
#ifdef _WIN32
            sockaddr_in addr;
            int len = sizeof(addr);

            auto result = getsockname(socket.native(), (sockaddr*)&addr, &len);

            if (result == SOCKET_ERROR)
            {
                throw network_error("upd_ip4_socket::endpoint()", get_os_network_error_code());
            }
            assert(len == sizeof(addr));
            return addr;
#else
            sockaddr_in addr;
                socklen_t len = sizeof(addr);

                auto result = ::getsockname(socket.native(), (native_sockaddr*)&addr, &len);

                if (result == -1)
                {
                    throw network_error("upd_ip4_socket::endpoint()", get_os_network_error_code());
                }
                assert(len == sizeof(addr));
                return addr;
#endif
        }
        inline ip4_udp_endpoint net_endpoint(async_ip4_udp_socket& socket)
        {
#ifdef _WIN32
            sockaddr_in addr;
            int len = sizeof(addr);

            auto result = getsockname(socket.native(), (sockaddr*)&addr, &len);

            if (result == SOCKET_ERROR)
            {
                throw network_error("upd_ip4_socket::endpoint()", get_os_network_error_code());
            }
            assert(len == sizeof(addr));
            return addr;
#else
            sockaddr_in addr;
                socklen_t len = sizeof(addr);

                auto result = ::getsockname(socket.native(), (native_sockaddr*)&addr, &len);

                if (result == -1)
                {
                    throw network_error("upd_ip4_socket::endpoint()", get_os_network_error_code());
                }
                assert(len == sizeof(addr));
                return addr;
#endif
        }

#endif
}

#endif // RPNXCORE_NETWORKING_BASIC_SYNC_API_HPP
