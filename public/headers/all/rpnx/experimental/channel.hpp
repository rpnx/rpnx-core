//
// Created by Ryan on 4/26/2021.
//

#ifndef RPNXCORE_CHANNEL_HPP
#define RPNXCORE_CHANNEL_HPP
#include <atomic>
#include <condition_variable>
#include <deque>
#include <list>
#include <memory>
#include <optional>
#include <set>
#include <vector>
#include <iostream>
#include <syncstream>

namespace rpnx::experimental
{
    template <typename T >
    class channel;

    template <typename T >
    class associated_channel;


    template <typename T>
    class auto_channel_processor;

    template <typename T>
    void register_channel(channel<T> & channel, auto_channel_processor<T> &processor);

    template <typename T>
    void unregister_channel(channel<T> & channel, auto_channel_processor<T> &processor);


    template <typename T>
    void register_channel(channel<T> & channel, auto_channel_processor<T> &processor)
    {

        std::unique_lock v_lck(channel.m_mtx);
        assert(channel.m_registered == nullptr);
        channel.m_registered = &processor;


        if (!channel.m_queue.empty())
        {
            std::unique_lock v_lock2 (processor.m_mtx);
            processor.m_active_set.insert(&channel);
            processor.m_cond.notify_all();
        }
    }

    template <typename T>
    void unregister_channel(channel<T> & channel, auto_channel_processor<T> &processor)
    {
        std::unique_lock v_lck(channel.m_mtx);
        assert(channel.m_registered == &processor);
        channel.m_registered = nullptr;
        v_lck.unlock();

        // When this call completes we guarantee that there are no more running jobs
        // from that channel, so we need to wait for any running jobs to complete.
        std::unique_lock v_lock2 (processor.m_mtx);
        processor.m_active_set.erase(&channel);
        processor.m_cond.wait(v_lock2,
                              [&]{
                                bool can_quit = true;
                                for (int i = 0; i < processor.m_job_list.size(); i++)
                                {
                                    if (processor.m_job_list[i].m_source == &channel)
                                    {
                                        can_quit = false;
                                        processor.m_job_list[i].m_should_notify = true;
                                    }
                                }
                                return can_quit;
                              });
    }

    /** Channel is a thread-safe queue that allows for some actions to be taken upon receipt
     * asynchronously.
     *
     */
    template <typename T >
    class channel
    {
        friend void register_channel<>(channel<T> & channel, auto_channel_processor<T> &processor);
        friend void unregister_channel<>(channel<T> & channel, auto_channel_processor<T> &processor);
        friend class associated_channel<T>;

        // TODO: Basic channel
        std::mutex m_mtx;
        std::condition_variable m_cond;
        std::deque< std::unique_ptr<T> > m_queue;
        auto_channel_processor<T> * m_registered = nullptr;

      public:
        channel() {}
        channel(channel<T> const &) = delete;
        channel(channel<T> &&) = delete;

        /** Sends a value over a channel
         * This function is safe to call concurrently.
         *
         * @param value The object to send (by value)
         */
        void send(T value)
        {
            std::unique_ptr<T> ptr = std::make_unique<T>(std::move(value));

            std::unique_lock v_lock(m_mtx);
            m_queue.push_back(std::move(ptr));

            if (m_queue.size() == 1)
            {
                m_cond.notify_all();
                if (m_registered)
                {
                    std::unique_lock v_lock2(m_registered->m_mtx);
                    m_registered->m_active_set.insert(this);
                    m_registered->m_cond.notify_all();
                }
            }

        }

       /** Attemps to receive a value from the channel.
        * The value received is removed from the channel.
        * @return If the channel is empty, than std::nullopt, otherwise, the oldest value in the channel
        */
        std::optional<T> try_receive()
        {
            std::unique_lock v_lock(m_mtx);
            if (m_queue.empty()) return std::nullopt;
            std::unique_ptr<T> t = nullptr;
            t = std::move(m_queue.front());
            m_queue.pop_front();
            T result(std::move(*t));
            if (m_queue.empty() && m_registered)
            {
                std::unique_lock v_lock2(m_registered->m_mtx);
                m_registered->m_active_set.erase(this);
                m_registered->m_cond.notify_all();
            }
            return result;
        }

        /** Receive a value from the channel.
         * Blocks until the channel is not empty.
         *
         * @return The value taken from the channel.
         */
        T receive()
        {
            std::unique_lock v_lock(m_mtx);
            m_cond.wait(v_lock, [&]{ return ! m_queue.empty(); });
            std::unique_ptr<T> t = std::move(m_queue.front());
            m_queue.pop_front();
            T result(std::move(*t));
            if (m_queue.empty() && m_registered)
            {
                std::unique_lock v_lock2(m_registered->m_mtx);
                m_registered->m_active_set.erase(this);
                m_registered->m_cond.notify_all();
            }
            return result;
        }

        std::unique_ptr<T> receive_ptr()
        {
            std::unique_lock v_lock(m_mtx);
            m_cond.wait(v_lock, [&]{ return ! m_queue.empty(); });
            std::unique_ptr<T> t = std::move(m_queue.front());
            m_queue.pop_front();
            if (m_queue.empty() && m_registered)
            {
                std::unique_lock v_lock2(m_registered->m_mtx);
                m_registered->m_active_set.erase(this);
                m_registered->m_cond.notify_all();
            }
            return std::move(t);
        }

        /**
         * Pulls an item if the channel is not empty, otherwise returns nullptr.
         */
        std::unique_ptr<T> try_receive_ptr()
        {
            std::unique_lock v_lock(m_mtx);
            if (m_queue.empty()) return nullptr;
            std::unique_ptr<T> t = std::move(m_queue.front());
            m_queue.pop_front();
            if (m_queue.empty() && m_registered)
            {
                std::unique_lock v_lock2(m_registered->m_mtx);
                m_registered->m_active_set.erase(this);
                m_registered->m_cond.notify_all();
            }
            return std::move(t);
        }
    };

    template <typename T>
    class auto_channel_processor
    {
        friend class channel<T>;
        friend void register_channel<>(channel<T> & channel, auto_channel_processor<T> &processor);
        friend void unregister_channel<>(channel<T> & channel, auto_channel_processor<T> &processor);
        struct active_job
        {
            channel<T> * m_source = nullptr;
            bool m_should_notify = false;
        };
        std::mutex m_mtx;
        std::condition_variable m_cond;
        std::function<void(T)> m_process;
        std::set<channel<T>*> m_active_set;
        // Figure out a way to do this that doesn't cause certain set items to be prioritized
        bool m_should_stop = false;
        std::vector<active_job> m_job_list;
        std::vector<std::thread> m_run_thread;
      public:
        auto_channel_processor(std::function<void(T)> processor, int jobs = 1)// std::thread::hardware_concurrency())
            : m_process(processor)
        {
            m_job_list.resize(jobs);
            if (m_job_list.empty())
            {
                m_job_list.resize(1);
            }

            for (int i = 0; i < m_job_list.size(); i++)
            {
                m_run_thread.emplace_back(std::thread([i, this] {  run_thread(i); }));
            }
        }
        auto_channel_processor(auto_channel_processor const &) = delete;
        ~auto_channel_processor()
        {
            std::unique_lock v_lck(m_mtx);
            m_should_stop = true;
            m_cond.notify_all();
            v_lck.unlock();
            for (std::thread & th : m_run_thread)
            {
                th.join();
            }
        }
      private:
        void run_thread(int i)
        {
            while(true)
            {
                std::unique_lock v_lck(m_mtx);
                m_cond.wait(v_lck, [&] { return m_should_stop || ! m_active_set.empty() || m_job_list[i].m_should_notify; });
                if (m_should_stop) return;
                auto it = m_active_set.begin();
                assert(it != m_active_set.end());
                channel<T> * v_channel = *it;
                m_job_list[i].m_source = v_channel;
                m_job_list[i].m_should_notify = false;
                v_lck.unlock();

                std::unique_ptr<T> ptr = v_channel->try_receive_ptr();
                if (ptr != nullptr)
                {
                    //std::cout << "non spurious wakeup" << std::endl;
                    m_process(std::move(*ptr));
                }
                v_lck.lock();
                m_cond.notify_all();
                m_job_list[i].m_should_notify = false;
                m_job_list[i].m_source = nullptr;
                v_lck.unlock();
                continue;
            }
        }
    };



    template <typename T>
    class channel_registration_binder
    {
        channel<T> & mc;
        auto_channel_processor<T> &pm;
      public:
        inline channel_registration_binder(channel<T> & channel, auto_channel_processor<T> &processor)
        : mc(channel), pm(processor)
        {
            register_channel(mc, pm);
        }
        ~channel_registration_binder()
        {
            unregister_channel(mc, pm);
        };

    };

    template <typename T>
    class associated_channel
    : public channel<T>
    {
      public:
        associated_channel(auto_channel_processor<T> & binding)
        {
            register_channel(*this, &binding);
        }

        ~associated_channel()
        {
            unregister_channel(*this, this->m_registered );
        }
    };

    template <typename T>
    class channel_submitter
    {
        channel<T> & m_ref;
      public:
        channel_submitter(channel<T> & chan) : m_ref(chan) {}

        void operator()(T val) const
        {
           // std::osyncstream (std::cerr) << "Output from " __FUNCTION__ << std::endl;
            m_ref.send(std::move(val));
        }
    };

    template <typename T, typename T2>
    class channel_pairing_submitter
    {
        channel<std::pair<T, T2>> & m_ref;
        T2 m_extra;
      public:
        channel_pairing_submitter(channel<std::pair<T, T2>> & chan, T2 t2) : m_ref(chan), m_extra(std::move(t2)) {}

        void submit(T val)
        {
            m_ref.send(std::make_pair<T, T2>(std::move(val), m_extra));
        }
    };
}

#endif // RPNXCORE_CHANNEL_HPP
