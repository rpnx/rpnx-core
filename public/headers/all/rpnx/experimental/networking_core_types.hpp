//
// Created by rnicholl on 1/8/2022.
//

#ifndef RPNXCORE_NETWORKING_CORE_TYPES_HPP
#define RPNXCORE_NETWORKING_CORE_TYPES_HPP

#include "rpnx/experimental/networking_core_types_fwd.hpp"
#include "rpnx/experimental/networking_core_types_refs.hpp"
#include "rpnx/experimental/networking_core_types_basic.hpp"

namespace rpnx::experimental
{
    class ip6_address
    {
#ifdef _WIN32
        IN6_ADDR m_data;
#else
        in6_addr m_data;
#endif
      public:
        constexpr ip6_address() noexcept
            : m_data{}
        { }

        constexpr ip6_address(ip6_address const &) = default;
        constexpr std::size_t size() const { return 16; }

#ifdef _WIN32
        constexpr ip6_address(IN6_ADDR const & other)
            : m_data{}
        {
            for (int i = 0; i < size(); i++)
            {
                m_data.u.Byte[i] = *(reinterpret_cast<unsigned char const*>(&other)+i);
            }
        }

        inline constexpr IN6_ADDR const& native() const
        {
            return m_data;
        }


        constexpr std::uint8_t & operator[](int index)
        {
            //RPNX_ASSERT(index < size());
            return m_data.u.Byte[index];
        }

        constexpr std::uint8_t const & operator[](int index) const noexcept
        {
            //RPNX_ASSERT(index < size());
            return m_data.u.Byte[index];
        }




#else
        inline constexpr in6_addr const & native() const noexcept
        {
            return m_data;
        }
#endif

        // TODO: Check that this generates good instructions on x86
        bool operator ==(const ip6_address & other) const noexcept
        {
            for (int i = 0; i < size(); i++)
            {
                if (this->operator[](i) != other[i])
                {
                    return false;
                }
            }
            return true;
        }


    };

    class ip6_tcp_endpoint
    {
        ip6_address m_addr;
        std::uint16_t m_port;
      public:

        ip6_tcp_endpoint()  noexcept
            : m_addr(), m_port(0)
        {}

        ip6_tcp_endpoint(ip6_tcp_endpoint const &) = default;

        ip6_tcp_endpoint(ip6_address const & addr, std::uint16_t port)
            : m_addr(addr), m_port(port)
        {}


        ip6_tcp_endpoint & operator = (ip6_tcp_endpoint const &) = default;

        bool operator==(ip6_tcp_endpoint const & other) const noexcept
        {
            return address() == other.address() && port() == other.port();
        }



        ip6_address& address()
        {
            return m_addr;
        }
        std::uint16_t& port()
        {
            return m_port;
        }

        ip6_address const& address() const
        {
            return m_addr;
        }
        std::uint16_t const& port() const
        {
            return m_port;
        }

#ifdef _WIN32
        // This native structure contains implementation fields that are not part of the endpoint
        // So it isn't a 1:1 correspondence, hence why the endpoint object does not
        // contain a sockaddr_in6
        sockaddr_in6 to_sockaddr_native() const
        {
            sockaddr_in6 result {};
            result.sin6_family = AF_INET6;
            result.sin6_addr = address().native();
            result.sin6_port = htons(port());
            return result;
        }
#endif


    };
    class ip4_address
    {
      private:
        using data_type = std::array< std::uint8_t, 4 >;
        data_type m_addr;

      public:
        ip4_address() noexcept : m_addr{0, 0, 0, 0}
        {
        }

        ip4_address(std::uint8_t a, std::uint8_t b, std::uint8_t c, std::uint8_t d) noexcept : m_addr{a, b, c, d}
        {
        }

        explicit ip4_address(in_addr const& addr)
        {
#ifdef _WIN32
            m_addr[0] = addr.S_un.S_un_b.s_b1;
            m_addr[1] = addr.S_un.S_un_b.s_b2;
            m_addr[2] = addr.S_un.S_un_b.s_b3;
            m_addr[3] = addr.S_un.S_un_b.s_b4;
#elif defined(__linux__) || defined(__APPLE__)
            m_addr[0] = reinterpret_cast< char const* >(&addr.s_addr)[0];
            m_addr[1] = reinterpret_cast< char const* >(&addr.s_addr)[1];
            m_addr[2] = reinterpret_cast< char const* >(&addr.s_addr)[2];
            m_addr[3] = reinterpret_cast< char const* >(&addr.s_addr)[3];
#else
#error Implement
#endif
        }

        inline in_addr native() const noexcept
        {

#ifdef _WIN32
            in_addr addr;
            addr.S_un.S_un_b.s_b1 = m_addr[0];
            addr.S_un.S_un_b.s_b2 = m_addr[1];
            addr.S_un.S_un_b.s_b3 = m_addr[2];
            addr.S_un.S_un_b.s_b4 = m_addr[3];
            return addr;
#endif
#if defined(__linux__) || defined(__APPLE__) || defined(__FreeBSD__)
            return {static_cast< in_addr_t >(m_addr[0] | (m_addr[1] << 8) | (m_addr[3] << 16) | (m_addr[3] << 24))};
#endif
        }

        using value_type = data_type::value_type;
        using iterator = data_type::iterator;
        using const_iterator = data_type::const_iterator;
        using pointer = data_type::pointer;
        using const_pointer = data_type::const_pointer;
        using reference = data_type::reference;
        using const_reference = data_type::const_reference;

        inline value_type& operator[](int n)
        {
            return m_addr[n];
        }

        inline value_type const& operator[](int n) const
        {
            return m_addr[n];
        }

        iterator begin()
        {
            return std::begin(m_addr);
        }
        const_iterator begin() const
        {
            return std::begin(m_addr);
        }

        iterator end()
        {
            return std::end(m_addr);
        }
        const_iterator end() const
        {
            return std::end(m_addr);
        }

        static ip4_address any()
        {
            ip4_address addr;
            addr[0] = 0;
            addr[1] = 0;
            addr[2] = 0;
            addr[3] = 0;
            return addr;
        }
    };

    class ip4_tcp_endpoint
    {
        ip4_address m_addr;
        uint16_t m_port;

      public:
        inline ip4_tcp_endpoint() : m_addr(), m_port(0)
        {
        }
        inline ip4_tcp_endpoint(ip4_address addr, uint16_t port) : m_addr(addr), m_port(port)
        {
        }

        inline explicit ip4_tcp_endpoint(sockaddr_in const& in)
        {
            assert(in.sin_family == AF_INET);
            m_port = ntohs(in.sin_port);
            m_addr = ip4_address(in.sin_addr);
        }

        sockaddr_in to_native() const
        {
            sockaddr_in addr;
            addr.sin_family = AF_INET;
            addr.sin_port = htons(m_port);
            addr.sin_addr = m_addr.native();
            return addr;
        }

        sockaddr_in to_sockaddr_native() const
        {
            return to_native();
        }

        ip4_address& address()
        {
            return m_addr;
        }
        uint16_t& port()
        {
            return m_port;
        }

        ip4_address const& address() const
        {
            return m_addr;
        }
        uint16_t const& port() const
        {
            return m_port;
        }
    };

    class ip4_udp_endpoint
    {
        ip4_address m_addr;
        uint16_t m_port;

      public:
        inline ip4_udp_endpoint() : m_port(0), m_addr()
        {
        }
        inline ip4_udp_endpoint(sockaddr_in const& native_addr) : ip4_udp_endpoint()
        {
            m_port = ntohs(native_addr.sin_port);
#ifdef _WIN32
            m_addr[0] = native_addr.sin_addr.S_un.S_un_b.s_b1;
            m_addr[1] = native_addr.sin_addr.S_un.S_un_b.s_b2;
            m_addr[2] = native_addr.sin_addr.S_un.S_un_b.s_b3;
            m_addr[3] = native_addr.sin_addr.S_un.S_un_b.s_b4;
#elif defined(__linux__) || defined(__APPLE__) || defined(__FreeBSD__)
            m_addr[0] = native_addr.sin_addr.s_addr >> 24;
            m_addr[1] = native_addr.sin_addr.s_addr >> 16;
            m_addr[2] = native_addr.sin_addr.s_addr >> 8;
            m_addr[3] = native_addr.sin_addr.s_addr >> 0;
#else
#error Not implemented
#endif
        }

        inline ip4_udp_endpoint(ip4_address addr, uint16_t port) : m_addr(addr), m_port(port)
        {
        }

        inline ip4_udp_endpoint(ip4_udp_endpoint const&) = default;

        explicit inline ip4_udp_endpoint(ip4_tcp_endpoint const& other) : m_addr(other.address()), m_port(other.port())
        {
        }

        sockaddr_in native() const
        {
            sockaddr_in addr;
            addr.sin_family = AF_INET;
            addr.sin_port = htons(m_port);
            addr.sin_addr = m_addr.native();
            return addr;
        }

        ip4_address& address()
        {
            return m_addr;
        }
        uint16_t& port()
        {
            return m_port;
        }

        ip4_address const& address() const
        {
            return m_addr;
        }
        uint16_t const& port() const
        {
            return m_port;
        }
    };


}

#endif // RPNXCORE_NETWORKING_CORE_TYPES_HPP
