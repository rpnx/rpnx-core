//
// Created by rnicholl on 1/8/2022.
//

#ifndef RPNXCORE_NETWORKING_TYPES_ASYNC_SERVICE_HPP
#define RPNXCORE_NETWORKING_TYPES_ASYNC_SERVICE_HPP

#include <vector>
#include <functional>
#include "rpnx/experimental/networking_core_types.hpp"
#include "rpnx/experimental/result.hpp"
#include "rpnx/experimental/networking_core_types_refs.hpp"
#include "rpnx/experimental/sockets_from_native.hpp"

namespace rpnx::experimental

{
#ifdef _WIN32

    template < typename T >
    struct actions_type
    {
        std::function< void(rpnx::experimental::result< T >) > m_mainaction;
        std::vector< std::function< void() > > m_auxactions;

        actions_type(std::function< void(rpnx::experimental::result< T >) > ma, 
            std::vector< std::function< void() > > ax) 
            : m_mainaction(std::move(ma)), m_auxactions(std::move(ax))
        {
        }

    };

    template < typename Ev >
    struct io_actor
    {

        union data {
            void * ptr;
            alignas(void*) std::array<std::byte, 24> buf;
        };
        struct vtable
        {
            void (*m_copy_construct_object)(void const* source, data* buf);
            void * (*m_ref)(data*);
            void (*m_move_construct_other) (data * a_source, data * a_dest);
            void (*m_move_construct_object) (data * a_source, data * a_dest);
            void (*m_destroy) (data * a_obj);

        };

        data m_data;
        vtable * m_vtab;
      public:
        template <typename T>
        static constexpr bool type_is_sso_ok()
        {
            return sizeof(T) <= 24 && alignof(T) <= alignof(decltype(data::buf))  && std::is_nothrow_move_constructible_v<T>;
        }

        static vtable build_vtable_for_void()
        {
            vtable result;
            result.m_copy_construct_object = +[](void const * a_source, data * a_buf) {};
            result.m_ref = +[](data * a_buf) -> void* {
                throw std::invalid_argument("Object is null");
            };
            result.m_destroy = +[](data*){};
        }


        template <typename T>
        static vtable build_vtable_for()
        {
            if constexpr (std::is_void_v<T>) return build_vtable_for_void();
            vtable result;
            result.m_copy_construct_object = +[](void const * a_source, data * a_buf)
            {
                T const * v_source = reinterpret_cast<T const *>(a_source);
                if constexpr (type_is_sso_ok<T>())
                {
                    new (a_buf->buf.data()) T(*v_source);
                }
                else
                {
                    a_buf->ptr = new T(*v_source);
                }
            };
            result.m_ref = [](data * a_buf) -> void*
            {
                if constexpr (type_is_sso_ok<T>())
                {
                    return  (void*) std::launder((T *) a_buf->buf.data());
                }
                else
                {
                    return a_buf->ptr;
                }
            };
            result.m_move_construct = [](data * a_source, data * a_dest)
            {
                T * v_source {};
                if constexpr (type_is_sso_ok<T>())
                {
                    v_source = std::launder((T *) a_source->buf.data());
                    new (a_dest->buf.data()) T(std::move(*v_source));
                }
                else
                {
                    v_source = (T*) a_source->ptr;
                    a_dest->ptr = new T(std::move(*v_source));
                }
            };
        }

        template <typename T>
        inline static constexpr vtable s_vtable_for = build_vtable_for<T>();
      public:


        io_actor()
            : m_vtab(&s_vtable_for<void>)
        {

        }

        ~io_actor()
        {
            m_vtab->m_destroy(&m_data);
        }

        void reset()
        {
            m_vtab->m_destroy(&m_data);
            m_vtab = &s_vtable_for<void>;
        }

        template <typename T>
        io_actor & operator=(T && value)
        {
            reset();
            m_vtab = &s_vtable_for<std::remove_reference_t<T> >;
            try
            {
                if constexpr (std::is_rvalue_reference_v<T>)
                {
                    m_vtab->m_move_construct_object(reinterpret_cast<void *>(&value), &m_data);
                }
                else
                {
                    m_vtab->m_copy_construct_object(reinterpret_cast<void *>(&value), &m_data);
                }
            }
            catch (...)
            {
                m_vtab = &s_vtable_for<void>;
                throw;
            }
            return *this;
        }



    };

    namespace detail
    {

        struct async_ip6_tcp_autoacceptor_binding;
        struct async_ip6_tcp_autoreceiver_binding;


    } // namespace detail



#endif
    class async_service;

    /** The async_service class implements an asynchronous service.
     * Internally, it's just a pointer to the actual implementation, so that the internal structure can be changed for new versions of the library.
     */
    class async_service
    {
       // template <typename Spec>
       // friend class async_tcp_autoacceptor<Spec>;
      private:
        void * m_pimpl;


      public:
        async_service();
        async_service(async_service const &) = delete;
        async_service(async_service && ) = delete;
        ~async_service();

        using transfer_block = std::vector<std::byte>;


      public:

        template <typename Spec>
        void * bind_autoaccept_spec(typename net_tcp_implementation<Spec>::acceptor_ref ref, actions_type< typename net_tcp_implementation<Spec>::connection > actions );

        template <typename Spec>
        void * bind_auto_receive_unlimited_spec(typename net_tcp_implementation<Spec>::connection_ref ref, actions_type< transfer_block > actions);

        template <typename Spec>
        void * bind_auto_ordered_send_unlimited_spec(typename net_tcp_implementation<Spec>::connection_ref ref, actions_type< std::size_t > actions);

        template <typename Spec>
        void submit_ordered_send_unlimited(void *, transfer_block);

        template <typename Spec>
        void unbind_autoaccept_spec(void *);

        template <typename Spec>
        void unbind_autoreceive_unlimited_spec(void * ptr);

        template <typename Spec>
        void unbind_auto_ordered_send_unlimited_spec(void * ptr);


    };


    //extern template void* async_service::bind_autoaccept_spec<async_ip6_tcp_specs>(typename net_tcp_implementation<async_ip6_tcp_specs>::acceptor_ref,  actions_type< typename net_tcp_implementation<async_ip6_tcp_specs>::connection >);
}

#endif // RPNXCORE_NETWORKING_TYPES_ASYNC_SERVICE_HPP
