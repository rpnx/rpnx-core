//
// Created by rnicholl on 1/8/2022.
//

#ifndef RPNXCORE_NETWORKING_CORE_TYPES_BASIC_HPP
#define RPNXCORE_NETWORKING_CORE_TYPES_BASIC_HPP
namespace rpnx
{
    namespace experimental
    {
        namespace detail
        {
#ifdef _WIN32
            class wsa_intializer
            {
              private:
                WSADATA wsaData;

              public:
                wsa_intializer(int a, int b)
                {
                    auto result = WSAStartup(MAKEWORD(a, b), &wsaData);
                    if (result != 0)
                        throw std::system_error(std::error_code(result, std::system_category()), "WSA intialization failed");
                }
                ~wsa_intializer()
                {
                    WSACleanup();
                }

                static wsa_intializer& singleton()
                {
                    static wsa_intializer wsa(2, 2);
                    return wsa;
                }
            };
#endif
        }
#ifdef _WIN32
        using native_socket_type = SOCKET;
        static const constexpr native_socket_type native_invalid_socket_value = INVALID_SOCKET;
#else
        using native_socket_type = int;
        static const constexpr native_socket_type native_invalid_socket_value = -1;
#endif
        using native_sockaddr_length_type = int;
        struct adopt_resource_t {};
        static const constexpr adopt_resource_t adopt_resource;
        struct transfer_lifetime_call_limited_tag{};
        struct transfer_lifetime_value_owned_tag{};
        struct transfer_lifetime_operation_shared_tag{};

        struct network_enabled_context
        {
#ifdef _WIN32
            network_enabled_context()
                : obj(2,2)
            {}
          private:
            detail::wsa_intializer obj;
#endif
        };
    }
}
#endif // RPNXCORE_NETWORKING_CORE_TYPES_BASIC_HPP
