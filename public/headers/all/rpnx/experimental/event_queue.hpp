//
// Created by rnicholl on 6/12/2022.
//

#ifndef RPNXCORE_EVENT_QUEUE_HPP
#define RPNXCORE_EVENT_QUEUE_HPP

#include "priority_dispatcher.hpp"
#include <functional>
#include <atomic>
#include <random>
#include <cassert>
#include "rpnx/experimental/thread_checker.hpp"

namespace rpnx::experimental
{

    /** Event Queue is a bit like a channel. It can accept events.
     * There are a few differences though, event_queues need to be attached
     * to event_handlers, which themselves need an execution stage.
     * @tparam Ev
     */
    template <typename Ev>
    class event_queue
    {
      private:
        std::mutex m_mtx;
        std::deque<Ev> m_events;
        thread_checker m_lock_cheker;
        std::optional<std::size_t> m_event_limit;
        std::vector< std::function<bool()> > m_on_not_full;
        std::vector< std::function<bool(bool)> > m_handlers_on_not_empty_edge_triggered;
        std::vector< std::function<bool(bool)> > m_handlers_on_full_edge_triggered;
        std::vector< std::function<bool(bool)> > m_handlers_on_not_full_edge_triggered;


        void run_handlers(std::vector<std::function<bool(bool)>> & evs, bool real)
        {
            for (std::size_t i = evs.size(); i != 0; i--)
            {
                std::size_t index = i-1;

                bool should_keep = evs[index](real);

                if (! should_keep)
                {
                    if (i != evs.size())
                    {
                        std::swap(evs[index], evs[evs.size()-1]);
                        evs.pop_back();
                    }
                }
            }
        }
      public:
        /** Registers a handler for when the event is not empty
         *
         * @param f A functor that accepts a boolean and returns a boolean. The function
         * receives a parameter of true if the event was triggered, or a parameter of false
         * if the event is spuriously triggered. The functor should return false if the event
         * handler should be erased afterwards, or true if the event handler should be kept. Spurious
         * wakeups of event handlers allow event_handlers to delete themselves in response to
         * user-defined events by calling spuriously_wake_handlers().
         *
         * Note that the order in which handlers are triggered is unspecified. In the current
         * implementation, they execute in FIFO order but if events are deleted then the order
         * can be scrambled because the deleted event is swapped with the last event, and then
         * the array is shrunk.
         *
         */
        void register_on_not_empty_handler(std::function<bool(bool)> f)
        {
            m_handlers_on_not_empty_edge_triggered.push_back(f);
        }


        void register_on_full_handler(std::function<bool(bool)> f)
        {
            m_handlers_on_full_edge_triggered.push_back(f);
        }

        void register_on_not_full_handler(std::function<bool(bool)> f)
        {
            m_handlers_on_not_full_edge_triggered.push_back(f);
        }

        /** Spuriously wakes the handlers attached to the event_queue. This is mainly to allow
         * handlers to delete themselves in response to user defined events, avoiding the need
         * for e.g. weak_ptr objects for events that might be triggered for objects that no
         * longer exist.
         *
         */
        void spuriously_wake_handlers()
        {
            run_handlers(m_handlers_on_not_empty_edge_triggered, false);
            run_handlers(m_handlers_on_full_edge_triggered, false);
            run_handlers(m_handlers_on_not_full_edge_triggered, false);
        }

        void lock()
        {
            m_mtx.lock();
        }

        void unlock()
        {
            m_mtx.unlock();
        }

        void event_limit(std::size_t n)
        {
            auto v = m_lock_cheker.start();
            m_event_limit = n;
            m_lock_cheker.stop(v);
        }

        void give_event(Ev e)
        {
            assert(m_events.size() < m_event_limit);
            // Mutex must be locked or undefined behavior occurs.
            m_events.push_back(std::move(e));
            if (m_events.size() == 1)
            {
                run_handlers(m_handlers_on_not_empty_edge_triggered, true);
            }
            if (!m_events.size() >= m_event_limit)
            {
                run_handlers(m_handlers_on_full_edge_triggered, true);
            }
        }

        Ev take_event()
        {
            assert(!m_events.empty());
            Ev ev = m_events.front();
            m_events.pop_front();

            if (m_event_limit.has_value() && m_events.size() == *m_event_limit - 1)
            {
                run_handlers(m_handlers_on_not_full_edge_triggered, true);
            }
            return std::move(ev);
        }
    };

    template <typename Ev>
    class event_queue_actor
    {
        event_queue< Ev > * m_q;
        bool m_dying = false;

      public:

        /** Destroys the event_queue_actor
         * @complexity where N is the number of events on the associated event_queue: O(N), plus any time spent in event handlers.
         * This is because the event_queue tests all events for validity
         */
        ~event_queue_actor()
        {
            lock();
            m_dying = true;
            m_q->spuriously_wake_handlers();
            unlock();
        }

        event_queue_actor(event_queue<Ev> & queue)
        : m_q(&queue)
        {


        }

        void lock()
        {
            m_q->lock();
        }


        /** Schedules a callback to notify the system when a receiver is ready to accept events.
         * @complexity where N is the number of related events on the associated event_queue: Worst case = O(N), average = O(1).
         * @tparam F Type of the function like object to schedule
         * @param f The functor to schedule
         */
        template <typename F>
        void schedule_notify_on_ready_once(F f)
        {
            m_q->m_handlers_on_not_empty_edge_triggered.push_back([f, this](bool ev){
                                                                      if (m_dying) return false;
                                                                      if (!ev) return true;
                                                                      f();
                                                                      return false;
                                                                  });
        }

        bool is_ready()
        {
            return m_q->m_events.size() != 0;
        }

        void unlock()
        {
            m_q->unlock();
        }

        /** Submits an event to the event_queue associated with this handler. Note that all events run in an I/O context and must i/o context safe.
         * @complexity Where H is the number of attached event_handlers matching triggered events, Ht is the time to execute event handlers, and N is the number of attached events,
         * Average case, no events triggered: O(1)
         * Worst case, no events triggered: O(N)
         * Average case, events triggered: O(H + Ht)
         * Worst case, evenets triggered: O(H + Ht + N)
         * @param e
         */
        void submit(Ev e)
        {
            m_q->give_event(std::move(e));
        }
    };


    template <typename Ev>
    class priority_event_handler
    {
      public:
        priority_event_handler(int priority, std::function<void(Ev)> handler , priority_dispatcher & dispatcher)
        {

        }
    };


}

#endif // RPNXCORE_EVENT_QUEUE_HPP
