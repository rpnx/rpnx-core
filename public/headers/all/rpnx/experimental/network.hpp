#ifndef RPNXCORE_NETWORK_HPP
#define RPNXCORE_NETWORK_HPP

#include <future>
#include "rpnx/assert.hpp"
#include "rpnx/experimental/channel.hpp"

#ifdef _WIN32
#include <WinSock2.h>
#include <in6addr.h>
#include "rpnx/kbind.hpp"
#endif

#if defined(__linux__) || defined(__APPLE__) || defined(__FreeBSD__)

#if defined(__FreeBSD__)
#include <sys/types.h>
#endif
#include <fcntl.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <sys/socket.h>
#include <unistd.h>
#endif

#if defined(__FreeBSD__)
#include <netinet/in.h>
#include <sys/socket.h>
#endif

#if defined(__linux__)
#include <sys/epoll.h>
#endif

#include <array>
#include <assert.h>
#include <climits>
#include <functional>
#include <iostream>
#include <system_error>
#include <vector>

#include "priority_dispatcher.hpp"
#include "result.hpp"
#include "rpnx/network_error.hpp"
#include <set>

#include "rpnx/experimental/networking_core_types.hpp"

#include "rpnx/experimental/networking_core_types_refs.hpp"
#include "rpnx/experimental/networking_types_async_service.hpp"
#include "rpnx/experimental/networking_api_autos.hpp"
#include "rpnx/experimental/networking_basic_sync_api.hpp"
#include "rpnx/experimental/networking_types_armos.hpp"
#include <cstring>

#include <map>
#include <optional>
#include <type_traits>
#include <utility>

namespace rpnx
{


    namespace experimental
    {
#if defined(__linux__) || defined(__APPLE__) || defined(__FreeBSD__)
        inline std::error_code get_os_network_error_code()
        {
            return std::error_code(errno, std::system_category());
        }
        using native_socket_type = int;
        using native_sockaddr_length_type = socklen_t;
        static const constexpr native_socket_type native_invalid_socket_value = -1;
        inline auto native_close_socket(native_socket_type skt)
        {
            return ::close(skt);
        }
#endif

        template <class T1, class T2>
        constexpr T1 bit_cast(T2 const& value) noexcept
        {
            static_assert(sizeof(T1) == sizeof(T2));
            static_assert(std::is_trivially_copyable_v<T1>);            
            static_assert(std::is_trivially_copyable_v<T2>);
            T1 t;
            std::memcpy(&t, &value, sizeof(value));
            return t;
        }


        template<typename Submitter, typename T>
        class action_channel_transfer
        {
            friend class async_service;
            Submitter & m_sub_ref;
            channel<T> & m_channel_ref;
          public:
            action_channel_transfer(Submitter & sub, channel<T> & chan)
            : m_sub_ref(sub), m_channel_ref(chan)
            {}
        };


        async_service& default_async_service();

        template <typename T>
        class try_action
        {
            T m_action;
            std::function<void(std::exception_ptr)> m_onerr;
          public:
            try_action(T action, std::function<void(std::exception_ptr)> onerr)
            : m_action(std::move(action)),
                  m_onerr(onerr)
            {

            }

            template <typename T2>
            void operator()(T2 value)
            {
                try
                {
                    m_action(std::move(value));
                }
                catch (...)
                {
                    std::exception_ptr ptr = std::current_exception();
                    m_onerr(ptr);
                }
            }
        };

    }; // namespace experimental
} // namespace rpnx

inline std::ostream& operator<<(std::ostream& lhs, rpnx::experimental::ip4_address const& addr)
{
    return lhs << (int)addr[0] << "." << (int)addr[1] << "." << (int)addr[2] << "." << (int)addr[3];
}

inline std::ostream& operator<<(std::ostream& lhs, rpnx::experimental::ip4_udp_endpoint const& ep)
{
    return lhs << ep.address() << ":" << ep.port();
}

inline std::ostream& operator<<(std::ostream& lhs, rpnx::experimental::ip4_tcp_endpoint const& ep)
{
    return lhs << ep.address() << ":" << ep.port();
}

#endif
