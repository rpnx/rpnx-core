//
// Created by rnicholl on 7/24/2022.
//

#ifndef RPNXCORE_IO_EVENT_QUEUE2_IOCTOR_HPP
#define RPNXCORE_IO_EVENT_QUEUE2_IOCTOR_HPP

namespace rpnx::experimental
{
    template <typename T, typename Alloc>
    class basic_event_queue_ioctor
    {
      public:
        void submit()
        {

        }
    };
} // namespace rpnx::experimental
}

#endif // RPNXCORE_IO_EVENT_QUEUE2_IOCTOR_HPP
