//
// Created by rnicholl on 7/24/2022.
//

#ifndef RPNXCORE_RAW_BISTATE_HANDLERS_HPP
#define RPNXCORE_RAW_BISTATE_HANDLERS_HPP

#include <iostream>
#include <mutex>
#include <set>
namespace rpnx::experimental
{

    namespace detail
    {
        /**
         * The following class implements a watcher for events.
         * When the event transitions to the active state, the m_enable function is called
         * When the event transitions to the inactive state, the m_disable function is called
         * The m_enable and m_disable functions are called with the context as the first argument
         *
         * The raw_bistate_handler may only be attached to 1 event at a time, and it must be detached
         * before it is destroyed.
         */
        struct raw_bistate_handler
        {
            std::mutex m_mtx;
            std::mutex* m_parent_mutex = nullptr;
            void* m_context = nullptr;
            using cb_type = void (*)(void*);

            cb_type m_enable = nullptr;
            cb_type m_disable = nullptr;
            cb_type m_detach = nullptr;
            ~raw_bistate_handler()
            {
                if (m_parent_mutex)
                {
                    std::cerr << "The behavior is undefined if an event_watch is destroyed without detaching it" << std::endl;
                    std::terminate();
                }
            }
        };

    } // namespace detail

    /**
     * The basic_bistate_source class is the base class for all event sources.
     * An event source can be set into signaled or unsignaled state.
     *
     * When the event source is enabled, any watchers that are attached to it will be set to the enabled state.
     * When the event source is disabled, any watchers that are attached to it will be set to the disabled state.
     *
     * @tparam Alloc The allocator to use internally.
     */
    template < typename Alloc >
    class basic_bistate_source
    {
        std::mutex m_mtx;
        bool m_active;
        std::set< detail::raw_bistate_handler*,
        std::less< detail::raw_bistate_handler* >,
             typename std::allocator_traits< Alloc >::template rebind_alloc< detail::raw_bistate_handler* >
                  >
            m_watched_by;

      public:
        basic_bistate_source(Alloc alloc = Alloc()) : m_watched_by(alloc)
        {
            m_active = false;
        }

        ~basic_bistate_source()
        {
            std::unique_lock lck(m_mtx);
            if (m_watched_by.size() != 0)
            {
                std::cerr << "The behavior is undefined if the event source is destroyed while it is still being watched" << std::endl;
                std::terminate();
            }
        }

        /**
         * Transitions the event source into the active state.
         * If the event source is already active, this function does nothing.
         * Otherwise, the enable callback on all event watchers which are watching this event source is triggered in the calling thread.
         */
        void signal()
        {
            std::unique_lock< std::mutex > lck(m_mtx);
            if (m_active)
            {
                return;
            }

            m_active = true;
            for (auto& watcher : m_watched_by)
            {
                std::unique_lock< std::mutex > lck2(watcher->m_mtx);
                watcher->m_enable(watcher->m_context);
            }
        }

        /**
         * Transitions the event source into the inactive state.
         * If the event source is already inactive, this function does nothing.
         * Otherwise, the disable callback on all event watchers which are watching this event source is triggered in the calling thread.
         */
        void reset()
        {
            std::unique_lock< std::mutex > lck(m_mtx);
            if (!m_active)
            {
                return;
            }

            m_active = false;
            for (auto& watcher : m_watched_by)
            {
                std::unique_lock< std::mutex > lck2(watcher->m_mtx);
                if (watcher->m_disable)
                {
                    watcher->m_disable(watcher->m_context);
                }
            }
        }

        /**
         * Returns true if the event source is active.
         * @return True if the event source is active. False otherwise.
         */
        bool active()
        {
            std::unique_lock< std::mutex > lck(m_mtx);
            return m_active;
        }

        void attach_raw_event_watch(detail::raw_bistate_handler* who)
        {
            std::unique_lock< std::mutex > lck(m_mtx);
            m_watched_by.insert(who);
            if (m_active)
            {
                who->m_parent_mutex = &m_mtx;
                std::unique_lock< std::mutex > lck2(who->m_mtx);
                who->m_enable(who->m_context);
            }
        }

        void detach_raw_event_watch(detail::raw_bistate_handler* who)
        {
            std::unique_lock< std::mutex > lck2(m_mtx);
            m_watched_by.erase(who);
            if (m_active)
            {
                std::unique_lock< std::mutex > lck(who->m_mtx);
                if (who->m_detach)
                {
                    who->m_detach(who->m_context);
                }
                who->m_parent_mutex = nullptr;
            }
        }
    };

    using bistate_source = basic_bistate_source< std::allocator< void* > >;

    template < typename Alloc, typename Pred >
    class basic_predicate_bistate_source
    {
        basic_bistate_source< Alloc > m_source;
        Pred m_pred;

      public:
        basic_predicate_bistate_source(Pred pr, Alloc const& alloc = Alloc()) : m_source(alloc), m_pred(pr)
        {
            check();
        }

        /** The following function checks the predicate and signals the source if true */
        void check()
        {
            if (m_pred())
            {
                m_source.signal();
            }
            else
            {
                m_source.reset();
            }
        }

        void attach_event_watch(detail::raw_bistate_handler* who)
        {
            m_source.attach_raw_event_watch(who);
        }

        void detach_event_watch(detail::raw_bistate_handler* who)
        {
            m_source.detach_raw_event_watch(who);
        }
    };

}; // namespace rpnx::experimental

#endif // RPNXCORE_RAW_BISTATE_HANDLERS_HPP
