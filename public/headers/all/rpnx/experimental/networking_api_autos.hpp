//
// Created by rnicholl on 1/8/2022.
//

#ifndef RPNXCORE_NETWORKING_API_AUTOS_HPP
#define RPNXCORE_NETWORKING_API_AUTOS_HPP
#include "rpnx/experimental/networking_api_autos_fwd.hpp"
#include "rpnx/experimental/networking_core_types.hpp"
#include "rpnx/experimental/networking_core_types_refs.hpp"

#include "rpnx/experimental/net_transfer_buffer.hpp"
#include "rpnx/experimental/networking_types_armos.hpp"
#include "rpnx/experimental/networking_types_async_service.hpp"
#include "rpnx/experimental/result.hpp"
#include <array>
#include <functional>
#include <vector>

namespace rpnx::experimental
{




    template <typename Spec>
    class async_tcp_autoacceptor
    {
        async_service & m_async;
        void* m_binding;

        static_assert(Spec::is_async, "Synchronous types not yet implemented");

      public:
        using acceptor_ref = typename net_tcp_implementation<Spec>::acceptor_ref;
        using result_type = result<typename net_tcp_implementation<Spec>::connection>;

        template <typename MainAction, typename ... AuxActions >
        [[maybe_unused]] async_tcp_autoacceptor( acceptor_ref socket
                                                    , async_service & async
                                                    , MainAction completion_action
                                                    , AuxActions && ... aux_actions
        );

        ~async_tcp_autoacceptor();
    };

    template <typename Spec>
    class async_tcp_auto_receiver_unlimited
    {
        async_service & m_async;
        void * m_binding;

      public:
        template <typename MainAction, typename ... AuxActions>
        [[maybe_unused]] async_tcp_auto_receiver_unlimited(typename net_tcp_implementation<Spec>::connection_ref c
                                                    , async_service & async
                                                    , MainAction completion_action
                                                    , AuxActions && ... aux_actions
        )
        : m_async(async)
        {
            std::vector< std::function< void() > > aux;

            //(0, aux.push_back(std::forward< AuxActions&& >(aux_actions)), ...);
            // TODO: Make aux actions work
            //
            actions_type< async_service::transfer_block > actions(std::move(completion_action), std::move(aux));

            m_binding = m_async.bind_auto_receive_unlimited_spec<Spec>(c, std::move(actions));
        }


        ~async_tcp_auto_receiver_unlimited()
        {
            m_async.unbind_autoreceive_unlimited_spec<Spec>(m_binding);
        }

        using transfer_type = std::vector<std::byte>;
        using transfer_tag = transfer_lifetime_value_owned_tag;
        using result_type = result<transfer_type>;
    };

    template <typename Spec>
    class async_tcp_auto_ordered_sender_unlimited
    {
        async_service & m_async;
        void * m_binding;

      public:
        template <typename MainAction, typename ... AuxActions>
        [[maybe_unused]] async_tcp_auto_ordered_sender_unlimited(typename net_tcp_implementation<Spec>::connection_ref c
                                                           , async_service & async
                                                           , MainAction completion_action
                                                           , AuxActions && ... aux_actions
                                                           )
            : m_async(async)
        {
            std::vector< std::function< void() > > aux;
            //(0, aux.push_back(std::forward< AuxActions&& >(aux_actions)), ...);
            // TODO: Make aux actions work
            //
            actions_type< std::size_t > actions(std::move(completion_action), std::move(aux));
            m_binding = m_async.bind_auto_ordered_send_unlimited_spec<Spec>(c, std::move(actions));
        }

        ~async_tcp_auto_ordered_sender_unlimited()
        {
            m_async.unbind_auto_ordered_send_unlimited_spec<Spec>(m_binding);
        }

        template <typename It>
        void send_async(It begin, It end)
        {
            async_service::transfer_block buf(begin, end);
            m_async.template submit_ordered_send_unlimited<Spec>(m_binding, std::move(buf));
        }

        using transfer_type = std::vector<std::byte>;
        using transfer_tag = transfer_lifetime_value_owned_tag;
        using result_type = result<transfer_type>;
    };

    template < typename Spec>
    template < typename MainAction , typename ... AuxActions >
    async_tcp_autoacceptor<Spec>::async_tcp_autoacceptor(typename async_tcp_autoacceptor<Spec>::acceptor_ref socket, async_service& async, MainAction completion_action, AuxActions&&... aux_actions) : m_async(async)
    {
        std::vector< std::function< void() > > aux;
        //(0, aux.push_back(std::forward< AuxActions&& >(aux_actions)), ...);
        // TODO: Make aux actions work
        actions_type< typename net_tcp_implementation<Spec>::connection > actions(std::move(completion_action), std::move(aux));
        m_binding = m_async.bind_autoaccept_spec<Spec>(socket, std::move(actions));
    }

    template < typename Spec>
    inline async_tcp_autoacceptor<Spec>::~async_tcp_autoacceptor()
    {
        m_async.unbind_autoaccept_spec<Spec>(this->m_binding);
    }



    using async_ip6_tcp_autoacceptor = async_tcp_autoacceptor<async_ip6_tcp_specs>;
    using async_ip4_tcp_autoacceptor = async_tcp_autoacceptor<async_ip4_tcp_specs>;

    using async_ip6_tcp_auto_receiver_unlimited = async_tcp_auto_receiver_unlimited<async_ip6_tcp_specs>;
    using async_ip4_tcp_auto_receiver_unlimited = async_tcp_auto_receiver_unlimited<async_ip4_tcp_specs>;

    
    using async_ip4_tcp_auto_ordered_sender_unlimited = async_tcp_auto_ordered_sender_unlimited<async_ip4_tcp_specs>;
    using async_ip6_tcp_auto_ordered_sender_unlimited = async_tcp_auto_ordered_sender_unlimited<async_ip6_tcp_specs>;
    
}



#endif // RPNXCORE_NETWORKING_API_AUTOS_HPP
