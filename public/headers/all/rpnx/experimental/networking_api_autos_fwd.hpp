//
// Created by rnicholl on 1/8/2022.
//

#ifndef RPNXCORE_NETWORKING_API_AUTOS_FWD_HPP
#define RPNXCORE_NETWORKING_API_AUTOS_FWD_HPP

namespace rpnx::experimental
{

    class async_ip6_tcp_auto_bytevector_multireceiver;

    template <typename Session, typename Data, typename Async>
    class async_ip6_tcp_server_autoconnector;
}


#endif // RPNXCORE_NETWORKING_API_AUTOS_FWD_HPP
