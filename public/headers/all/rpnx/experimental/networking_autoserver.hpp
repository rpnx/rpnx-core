//
// Created by rnicholl on 6/13/2022.
//

#ifndef RPNXCORE_NETWORKING_AUTOSERVER_HPP
#define RPNXCORE_NETWORKING_AUTOSERVER_HPP

#include "networking_api_autos.hpp"
#include "rpnx/experimental/sockets_from_native.hpp"

namespace rpnx::experimental
{
    template <typename Server, typename Session>
    class auto_server
    {
        Server s;


        using session_id = std::uint64_t;
        std::mutex m_mtx;

        session_id m_current_id = 1;
        async_service & m_svc;
        channel< result<async_ip4_tcp_connection> > m_ch_connections;
        channel< std::pair<result<async_service::transfer_block>, session_id> > m_ch_recv;

        struct ip4_tcp_session
        {
            Session m_user_session;
            async_ip4_tcp_connection m_connection;


            struct response_interface
            {
                ip4_tcp_session & m_ses;
                auto_server<Server, Session> & m_srv;
                session_id mv_id;

                template <typename It>
                void send(It begin, It end)
                {
                    send(current_session_id(), begin, end);
                }

                session_id current_session_id()
                {
                    return mv_id;
                }
            };

            ip4_tcp_session()
            {}

        };



        std::map<session_id, ip4_tcp_session> m_sessions;

        async_ip4_tcp_acceptor m_acceptor;
        async_ip4_tcp_autoacceptor m_autoacceptor;


      private:
        session_id allocate_session_id()
        {
            return m_current_id++;
        }

        void receive_connection_event(result< async_ip4_tcp_connection > conn)
        {
            std::unique_lock lck(m_mtx);

            auto next_session_id = allocate_session_id();
            m_sessions.emplace(std::forward_as_tuple(next_session_id, std::move(conn)), std::forward_as_tuple(next_session_id));

        }

      public:
        auto_server(async_service & svc)
        : m_svc(svc),
          m_acceptor(ip4_tcp_endpoint(ip4_address(127,0,0,1), 8080)),
              m_autoacceptor(m_acceptor, svc, rpnx::experimental::channel_submitter(m_ch_connections))
        {}



    };
}

#endif // RPNXCORE_NETWORKING_AUTOSERVER_HPP
