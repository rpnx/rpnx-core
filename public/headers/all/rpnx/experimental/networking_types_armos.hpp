//
// Created by rnicholl on 1/8/2022.
//

#ifndef RPNXCORE_NETWORKING_TYPES_ARMOS_HPP
#define RPNXCORE_NETWORKING_TYPES_ARMOS_HPP

#include "rpnx/experimental/networking_core_types_fwd.hpp"
#include "rpnx/experimental/networking_core_types_refs.hpp"

#include "rpnx/experimental/network_native_socket_types.hpp"
#include "rpnx/experimental/network_os_error_codes.hpp"


namespace rpnx::experimental
{




    class ip4_tcp_connection
    {
      private:
        native_socket_type m_socket;
      public:
        ip4_tcp_connection() noexcept : m_socket(native_invalid_socket_value)
        {
        }

#ifndef _WIN32
        // TODO: Implement this on Windows
        ip4_tcp_connection(ip4_tcp_endpoint const& ep)
        {
            m_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
            if (m_socket == -1)
            {
                throw network_error("ip4_tcp_connection::ip4_tcp_connection", get_os_network_error_code());
            }

            sockaddr_in addr = ep.native();
            int c = ::connect(m_socket, (sockaddr const*)&addr, sizeof(addr));
            if (m_socket == -1)
            {
                throw network_error("ip4_tcp_connection::ip4_tcp_connection", get_os_network_error_code());
            }
        }
#endif


        ip4_tcp_connection(ip4_tcp_connection const&) = delete;

        ip4_tcp_connection(ip4_tcp_connection&& other) noexcept : m_socket(native_invalid_socket_value)
        {
            std::swap(other.m_socket, m_socket);
        }

        /** Destructor of the socket. Either calls abort() or close() (it is unspecified which is called)
         */
        ~ip4_tcp_connection()
        {
            close();
            // TODO: force close the socket
        }

        /**
          Create a socket from a native SOCKET object.
          Ownership of the SOCKET is assumed by this class.
        */
        explicit inline ip4_tcp_connection(native_socket_type s) noexcept : m_socket(s)
        {
        }

        inline ip4_tcp_connection& operator=(ip4_tcp_connection&& other) noexcept
        {
            std::swap(other.m_socket, m_socket);
            return *this;
        }

        ip4_tcp_endpoint endpoint()
        {
            sockaddr_in saddr;
#ifdef _WIN32
            int sz = sizeof(saddr);
#else
            socklen_t sz = sizeof(saddr);
#endif
            if (getsockname(m_socket, (sockaddr*)&saddr, &sz) != 0)
            {
                throw network_error("ip4_tcp_connection::endpoint", get_os_network_error_code());
            }

            return ip4_tcp_endpoint(saddr);
        }

        ip4_tcp_endpoint peer_endpoint()
        {

            sockaddr_in saddr;
#ifdef _WIN32
            int sz = sizeof(saddr);
#else
            socklen_t sz = sizeof(saddr);
#endif
            if (getpeername(m_socket, (sockaddr*)&saddr, &sz) != 0)
            {
                throw network_error("peer_endpoint", get_os_network_error_code());
            }

            return ip4_tcp_endpoint(saddr);
        }

        /**
          Returns a copy of the internal SOCKET. This class retains ownership of the SOCKET.
        */
        [[nodiscard]] native_socket_type native() const noexcept
        {
            return m_socket;
        }

        /**
         Extracts the internal SOCKET. The function relinquishes ownership of the socket and the class's internal socket becomes INVALID_SOCKET.
        */
        [[nodiscard]] native_socket_type extract_native() noexcept
        {
            native_socket_type skt = m_socket;
            m_socket = native_invalid_socket_value;
            return skt;
        }

        [[nodiscard]] bool valid() const noexcept
        {
            return m_socket != -1;
        }

        template < typename It >

        void send(It begin, It end)
        {
            net_send(*this, begin, end);
        }

        /* TODO: Force close the socket
        void abort()
        {

        }*/

        void close()
        {
            if (m_socket != -1)
            {
#ifdef _WIN32
                shutdown(m_socket, SD_BOTH);
                closesocket(m_socket);
#else
                shutdown(m_socket, SHUT_RDWR);
                ::close(m_socket);
#endif
                m_socket = native_invalid_socket_value;
            }
        }
    };

    class async_ip4_udp_socket
    {
        friend void net_bind(async_ip4_udp_socket &, ip4_udp_endpoint const &);
      private:
        native_socket_type m_socket;

      public:



#ifndef _WIN32
        bool is_open() const { return m_socket != -1; }
        void open()
        {
            m_socket = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
            if (m_socket == -1)
            {
                throw std::runtime_error("SOCKET");
            }
        }

        [[nodiscard]] native_socket_type native() const
        {
            return m_socket;
        }
#endif


#ifdef _WIN32
        async_ip4_udp_socket()
            : m_socket(INVALID_SOCKET)
        {

        }

        bool is_open() const
        {
            return m_socket != -1;
        }

        void close()
        {
            if (is_open()) return;

            ::closesocket(m_socket);

            m_socket = native_invalid_socket_value;
        }

        [[nodiscard]] auto native() const
        {
            return m_socket;
        }

        void open()
        {
            if (is_open()) close();
            m_socket = WSASocketW(AF_INET, SOCK_DGRAM, IPPROTO_UDP, nullptr, 0, WSA_FLAG_OVERLAPPED);
            if (m_socket == -1)
            {
                throw std::runtime_error("WSA SOCKET");
            }
        }

        void bind(ip4_udp_endpoint ep)
        {
            net_bind(*this, ep);
        }

        void bind(ip4_address addr)
        {
            ip4_udp_endpoint ep;
            ep.address() = addr;
            ep.port() = 0;
            net_bind(*this, ep);
        }

        void bind(uint16_t portno)
        {
            ip4_udp_endpoint ep;
            ep.address() = ip4_address::any();
            ep.port() = portno;
            net_bind(*this, ep);
        }

        void bind()
        {
            ip4_udp_endpoint ep;
            ep.address() = ip4_address::any();
            ep.port() = 0;
            net_bind(*this, ep);
        }


#endif
    };
#if false
    class ip4_udp_socket
    {
      public:
        ip4_udp_socket_ref m_s;

      public:
        ip4_udp_socket() noexcept
        {
            m_s;
        }

        ip4_udp_socket(ip4_udp_socket const&) = delete;

        inline ip4_udp_socket(ip4_udp_socket&& other) noexcept : ip4_udp_socket()
        {
            std::swap(m_s, other.m_s);
        }

        ~ip4_udp_socket()
        {
            close();
        }

        void close()
        {
            if (m_s.is_null())
            {
                native_close_socket(m_s.native());
                m_s = {};
            }
        }

        native_socket_type native() const noexcept
        {
            return m_s;
        }

        operator ip4_udp_socket_ref() const noexcept
        {
            return m_s;
        }
    };
#endif


}

#endif // RPNXCORE_NETWORKING_TYPES_ARMOS_HPP
