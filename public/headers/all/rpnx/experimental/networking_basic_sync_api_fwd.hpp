//
// Created by rnicholl on 1/8/2022.
//

#ifndef RPNXCORE_NETWORKING_BASIC_SYNC_API_FWD_HPP
#define RPNXCORE_NETWORKING_BASIC_SYNC_API_FWD_HPP

#include "rpnx/experimental/networking_core_types.hpp"

namespace rpnx
{
    namespace experimental
    {
        template < typename It >
        void net_send(ip4_udp_socket_ref socket, ip4_udp_endpoint const& to, It input_begin, It input_end);

        template < typename It >
        void net_send(ip4_tcp_connection socket, It input_begin, It input_end);

        template < typename It >
        auto net_receive(ip4_udp_socket_ref socket, ip4_udp_endpoint& from, It output_begin, It output_bounds_check) -> It;

        template < typename It >
        auto net_receive(ip4_udp_socket_ref socket, ip4_udp_endpoint& from, It output_begin) -> It;

        template < typename It >
        auto net_receive(ip4_tcp_connection& socket, It output_begin, It output_end) -> void;

        template < typename It >
        auto net_receive(ip4_tcp_connection& socket, size_t count, It output) -> void;


        void net_bind(ip4_udp_socket_ref socket, ip4_udp_endpoint const& bind);
        void net_bind(async_ip4_udp_socket_ref socket, ip4_udp_endpoint const& bind);

        void net_listen(ip4_tcp_acceptor_ref socket);
        ip4_udp_endpoint net_endpoint(ip4_udp_socket& socket);
        ip4_udp_endpoint net_endpoint(async_ip4_udp_socket& socket);

        ip4_tcp_connection net_accept_connection(ip4_tcp_acceptor_ref);
        void net_accept_connection(ip4_tcp_acceptor& socket, ip4_tcp_connection& connection);

    }

}
#endif // RPNXCORE_NETWORKING_BASIC_SYNC_API_FWD_HPP
