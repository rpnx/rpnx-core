//
// Created by rnicholl on 1/8/2022.
//

#ifndef RPNXCORE_NETWORKING_CORE_TYPES_REFS_HPP
#define RPNXCORE_NETWORKING_CORE_TYPES_REFS_HPP

//#include "rpnx/experimental/networking_core_types_fwd.hpp"
#include "rpnx/experimental/networking_core_types_basic.hpp"


namespace rpnx
{
    namespace experimental
    {
        class [[maybe_unused]] ip4_tcp_connection_ref
        {
            friend class ip4_tcp_connection;

            native_socket_type m_socket;

          public:

            ip4_tcp_connection_ref() : m_socket(native_invalid_socket_value){}
            ip4_tcp_connection_ref(ip4_tcp_connection_ref const &) = default;

            native_socket_type native() const noexcept
            {
                return m_socket;
            }

            [[maybe_unused]] static inline ip4_tcp_connection_ref from_native(decltype(m_socket) native_value)
            {
                ip4_tcp_connection_ref result;
                result.m_socket = native_value;
                return result;
            }
        };

        class [[maybe_unused]] ip4_udp_socket_ref
        {
            native_socket_type m_socket;

          public:
            [[maybe_unused]] ip4_udp_socket_ref() : m_socket(native_invalid_socket_value) {}
            [[maybe_unused]] ip4_udp_socket_ref(ip4_udp_socket_ref const &) = default;


            auto native() const noexcept
            {
                return m_socket;
            }

            [[maybe_unused]] static inline ip4_udp_socket_ref from_native(native_socket_type native_value)
            {
                ip4_udp_socket_ref result;
                result.m_socket = native_value;
                return result;
            }

            operator bool() const noexcept
            {
                return !is_null();
            }

            bool is_null() const noexcept
            {
                return m_socket == native_invalid_socket_value;
            }
        };
        class [[maybe_unused]] async_ip4_udp_socket_ref
        {
            native_socket_type m_socket;

          public:
            [[maybe_unused]] async_ip4_udp_socket_ref() : m_socket(native_invalid_socket_value) {}
            [[maybe_unused]] async_ip4_udp_socket_ref(async_ip4_udp_socket_ref const &) = default;


            auto native() const noexcept
            {
                return m_socket;
            }

            [[maybe_unused]] static inline async_ip4_udp_socket_ref from_native(native_socket_type native_value)
            {
                async_ip4_udp_socket_ref result;
                result.m_socket = native_value;
                return result;
            }
        };

        class [[maybe_unused]] ip4_tcp_acceptor_ref
        {
            native_socket_type m_socket;

          public:
            [[maybe_unused]] ip4_tcp_acceptor_ref() : m_socket(native_invalid_socket_value) {}
            [[maybe_unused]] ip4_tcp_acceptor_ref(ip4_tcp_acceptor_ref const &) = default;


            auto native() const noexcept
            {
                return m_socket;
            }

            [[maybe_unused]] static inline ip4_tcp_acceptor_ref from_native(decltype(m_socket) native_value)
            {
                ip4_tcp_acceptor_ref result;
                result.m_socket = native_value;
                return result;
            }
        };
//        class [[maybe_unused]] async_ip6_tcp_connection_ref
//        {
//            native_socket_type m_socket;
//
//          public:
//            async_ip6_tcp_connection_ref() : m_socket(native_invalid_socket_value) {}
//            async_ip6_tcp_connection_ref(async_ip6_tcp_connection_ref const &) = default;
//            explicit async_ip6_tcp_connection_ref(native_socket_type value)
//                :m_socket(value){}
//
//
//
//            [[nodiscard]] native_socket_type native() const noexcept
//            {
//                return m_socket;
//            }
//        };

//        class async_ip6_tcp_acceptor_ref
//        {
//            native_socket_type m_socket;
//
//          public:
//            async_ip6_tcp_acceptor_ref() : m_socket(native_invalid_socket_value) {}
//            async_ip6_tcp_acceptor_ref(async_ip6_tcp_acceptor_ref const &) = default;
//            explicit async_ip6_tcp_acceptor_ref(native_socket_type value)
//                :m_socket(value){}
//
//
//            [[nodiscard]] native_socket_type native() const noexcept
//            {
//                return m_socket;
//            }
//        };

    }
}
#endif // RPNXCORE_NETWORKING_CORE_TYPES_REFS_HPP
