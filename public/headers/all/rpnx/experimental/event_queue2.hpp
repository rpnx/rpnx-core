//
// Created by rnicholl on 7/24/2022.
//

#ifndef RPNXCORE_EVENT_QUEUE2_HPP
#define RPNXCORE_EVENT_QUEUE2_HPP

#include "rpnx/experimental/raw_bistate_handlers.hpp"
#include <deque>

namespace rpnx::experimental
{
    template < typename T, typename Alloc >
    class basic_event_queue
    {
        std::mutex m_mtx;
        std::deque< T, typename std::allocator_traits< Alloc >::template rebind_alloc< T > > m_events;
        basic_bistate_source< Alloc > m_non_empty_bistate_source;

      public:
        basic_event_queue()
        {
        }

        basic_event_queue(Alloc const& alloc)
        : m_events(alloc)
        {
        }

        ~basic_event_queue()
        {
        }

        void send(T event)
        {
            std::unique_lock< std::mutex > lck(m_mtx);
            m_events.push_back(event);
            if (m_events.size() == 1)
            {
                m_non_empty_bistate_source.signal();
            }
        }

        bool try_take(T& output)
        {
            std::unique_lock< std::mutex > lck(m_mtx);
            if (m_events.size() == 0)
            {
                return false;
            }
            output = m_events.front();
            m_events.pop_front();
            if (m_events.size() == 0)
            {
                m_non_empty_bistate_source.reset();
            }
        }

        basic_bistate_source< Alloc >& non_empty_bistate_source()
        {
            return m_non_empty_bistate_source;
        }
    };

    template < typename T, typename Alloc >
    class basic_bounded_event_queue
    {
        std::mutex m_mtx;
        std::size_t m_max_size;
        basic_bistate_source< Alloc > m_non_empty_bistate_source;
        basic_bistate_source< Alloc > m_non_full_bistate_source;
        std::deque< T, Alloc > m_events;

      public:
        basic_bounded_event_queue(std::size_t n, Alloc const& alloc = Alloc()) : m_max_size(n), m_non_empty_bistate_source(alloc), m_non_full_bistate_source(alloc), m_events(alloc)

        {
            if (n == 0)
            {
                throw std::invalid_argument("n must be greater than 0");
            }
            m_non_full_bistate_source.reset();
            m_non_empty_bistate_source.signal();
        }

        ~basic_bounded_event_queue()
        {
        }

        basic_bistate_source< Alloc >& non_empty_bistate_source()
        {
            return m_non_empty_bistate_source;
        }

        basic_bistate_source< Alloc >& non_full_bistate_source()
        {
            return m_non_full_bistate_source;
        }

        void set_limit(std::size_t n)
        {
            std::unique_lock< std::mutex > lck(m_mtx);
            m_max_size = n;
            if (m_events.size() < m_max_size)
            {
                m_non_full_bistate_source.signal();
            }
            else
            {
                m_non_full_bistate_source.reset();
            }
        }

        bool try_send_copy(T const& t)
        {
            std::unique_lock< std::mutex > lck(m_mtx);
            if (m_events.size() >= m_max_size)
            {
                return false;
            }
            m_events.push_back(t);
            if (m_events.size() == 1)
            {
                m_non_empty_bistate_source.signal();
            }

            if (m_events.size() == m_max_size)
            {
                m_non_full_bistate_source.reset();
            }

            return true;
        }

        bool try_send_move(T& t)
        {
            std::unique_lock< std::mutex > lck(m_mtx);
            if (m_events.size() >= m_max_size)
            {
                return false;
            }
            m_events.push_back(std::move(t));
            if (m_events.size() == 1)
            {
                m_non_empty_bistate_source.signal();
            }

            if (m_events.size() == m_max_size)
            {
                m_non_full_bistate_source.reset();
            }

            return true;
        }
    };
} // namespace rpnx::experimental

#endif // RPNXCORE_EVENT_QUEUE2_HPP
