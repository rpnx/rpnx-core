//
// Created by rnicholl on 4/24/2022.
//

#ifndef RPNXCORE_NETWORK_OS_ERROR_CODES_HPP
#define RPNXCORE_NETWORK_OS_ERROR_CODES_HPP

#ifdef _WIN32
#include <utility>
#include <WinSock2.h>
#include <Windows.h>
#include <ws2ipdef.h>

namespace rpnx::experimental
{
    inline std::error_code get_os_network_error_code()
    {
        int er = WSAGetLastError();

        switch (er)

        {
        case WSA_NOT_ENOUGH_MEMORY:
            return std::make_error_code(std::errc::not_enough_memory);
        case WSA_INVALID_PARAMETER:
            return std::make_error_code(std::errc::invalid_argument);
        case WSA_OPERATION_ABORTED:
            return std::make_error_code(std::errc::operation_canceled);
        case WSA_IO_INCOMPLETE:
            return std::make_error_code(std::errc::operation_in_progress);
        case WSA_IO_PENDING:
            return std::make_error_code(std::errc::operation_in_progress);
        case WSAEINTR:
            return std::make_error_code(std::errc::interrupted);
        case WSAEBADF:
            return std::make_error_code(std::errc::bad_file_descriptor);
        case WSAEACCES:
            return std::make_error_code(std::errc::permission_denied);
        case WSAEFAULT:
            return std::make_error_code(std::errc::bad_address);
        case WSAEMFILE:
            return std::make_error_code(std::errc::too_many_files_open);
        case WSAEWOULDBLOCK:
            return std::make_error_code(std::errc::operation_would_block);
        case WSAEINPROGRESS:
            return std::make_error_code(std::errc::operation_in_progress);
        case WSAEALREADY:
            return std::make_error_code(std::errc::operation_in_progress);
        case WSAEMSGSIZE:
            return std::make_error_code(std::errc::message_size);
        case WSAEPFNOSUPPORT:
            return std::make_error_code(std::errc::protocol_not_supported);
        case WSAEADDRINUSE:
            return std::make_error_code(std::errc::address_in_use);
        case WSAEADDRNOTAVAIL:
            return std::make_error_code(std::errc::address_not_available);
        case WSAENETDOWN:
            return std::make_error_code(std::errc::network_down);
        case WSAENETUNREACH:
            return std::make_error_code(std::errc::network_unreachable);
        case WSAENETRESET:
            return std::make_error_code(std::errc::network_reset);
            // case WSAEACCES: return std::make_error_code(std::errc::permission_denied);
            // case WSAEACCES: return std::make_error_code(std::errc::permission_denied);
            //            return std::make_error_code(network_errc::permission_denied);
        }

        return std::error_code(er, std::system_category());
    }

}
#endif

#endif // RPNXCORE_NETWORK_OS_ERROR_CODES_HPP
