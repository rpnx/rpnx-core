//
// Created by rnicholl on 1/8/2022.
//

#ifndef RPNXCORE_NETWORKING_CORE_TYPES_FWD_HPP
#define RPNXCORE_NETWORKING_CORE_TYPES_FWD_HPP
namespace rpnx
{
    namespace experimental
    {
        struct network_enabled_context;
    // IPv4
      // TCP
        // Endpoints
        class ip4_udp_endpoint;
        // Sync
        class ip4_tcp_acceptor;
        class ip4_tcp_acceptor_ref;
        class ip4_tcp_connection;
        class ip4_tcp_connection_ref;
        // Async
//        class async_ip4_tcp_acceptor;
//        class async_ip4_tcp_acceptor_ref;
//        class async_ip4_tcp_connection;
//        class async_ip4_tcp_connection_ref;
      // UDP
        // Endpoints
        class ip4_udp_endpoint;
        // Sync
        class ip4_udp_socket;
        class ip4_udp_socket_ref;
        // Async
        class async_ip4_udp_socket;
        class async_ip4_udp_socket_ref;
    // IPv6
      // TCP
        // Endpoints
        class ip6_udp_endpoint;
        // Sync
        class ip6_tcp_acceptor;
        class ip6_tcp_acceptor_ref;
        class ip6_tcp_connection;
        class ip6_tcp_connection_ref;
        // Async
      //  class async_ip6_tcp_acceptor;
       // class async_ip6_tcp_acceptor_ref;
//        class async_ip6_tcp_connection;
//        class async_ip6_tcp_connection_ref;

      // UDP
        // Endpoints
        class ip6_tcp_endpoint;
        // Sync
        class ip6_udp_socket;
        class ip6_udp_socket_ref;
        // Async
        class async_ip6_udp_socket;
        class async_ip6_udp_socket_ref;
    }
}
#endif // RPNXCORE_NETWORKING_CORE_TYPES_FWD_HPP
