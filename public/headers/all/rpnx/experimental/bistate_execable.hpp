//
// Created by rnicholl on 7/2/2022.
//

#ifndef RPNXCORE_BISTATE_EXECABLE_HPP
#define RPNXCORE_BISTATE_EXECABLE_HPP

#include <list>
#include <mutex>

namespace rpnx

{
    /** The bistate_execable represents a class that has two states
     * Namely, the bistate_execable can either be in "ready" or
     * "not ready" state.
     * The class can be executed and might transition from "ready"
     * to "not ready" after doing so.
     *
     * The purpose of this class is to facilitate thread consolidation
     * and reduce context switches.
     */
    template <typename T>
    class bistate_watcher
    {
      struct state
      {
          T m_info;
      };
      std::mutex m_mtx;
      std::list<state> m_active;
    public:
      bistate_watcher()
      {
      }



    };

    template <typename T>
    class bistate_signaler
    {
        std::vector< bistate_watcher<T> * > m_watched_by;
        //std::optional<std::list<bistate_watcher<T>::state>::iterator> m_position;
    public:
        void activate(T value)
        {
        
        }
    };

}
#endif // RPNXCORE_BISTATE_EXECABLE_HPP
