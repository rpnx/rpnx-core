//
// Created by rnicholl on 6/15/2021.
//

#ifndef RPNXCORE_KBIND_HPP
#define RPNXCORE_KBIND_HPP

#include <MSWSock.h>
#include <WinSock2.h>
#include <cinttypes>
#include <ws2def.h>
#include <ws2ipdef.h>

namespace rpnx
{
    class kbind
    {
      public:
        kbind();
        LPFN_ACCEPTEX winnt_AcceptEx;
    };

    [[maybe_unused]]

    [[maybe_unused]] extern kbind g_kbind;
}

#endif // RPNXCORE_KBIND_HPP
