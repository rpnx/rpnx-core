#ifndef RPNX_NATIVE_SOCKET_TYPES_WINDOWS_HPP
#define RPNX_NATIVE_SOCKET_TYPES_WINDOWS_HPP
#include "rpnx/experimental/networking_core_types_fwd.hpp"
#include <WinSock2.h>
#include <Windows.h>
#include <ws2ipdef.h>

namespace rpnx::experimental
{
//    template < typename Socket >
//    struct native_socket_specs;


    struct async_ip6_tcp_specs
    {
        static constexpr int af = AF_INET6;
        static constexpr int type = SOCK_STREAM;
        static constexpr int protocol = IPPROTO_TCP;
        static constexpr DWORD dwFlags = WSA_FLAG_OVERLAPPED | WSA_FLAG_NO_HANDLE_INHERIT;
        static constexpr bool is_async = true;
        using native_sockaddr = sockaddr_in6;
        using native_address = IN6_ADDR;
        using native_socket = SOCKET;
        using endpoint = ip6_tcp_endpoint;
    };

    struct inheritable_async_ip6_tcp_specs
    {
        static constexpr int af = AF_INET6;
        static constexpr int type = SOCK_STREAM;
        static constexpr int protocol = IPPROTO_TCP;
        static constexpr DWORD dwFlags = WSA_FLAG_OVERLAPPED;
        static constexpr bool is_async = true;
        using native_sockaddr = sockaddr_in6;
        using native_address = IN6_ADDR;
        using native_socket = SOCKET;
        using endpoint = ip6_tcp_endpoint;
    };

    struct async_ip4_tcp_specs
    {
        static constexpr int af = AF_INET;
        static constexpr int type = SOCK_STREAM;
        static constexpr int protocol = IPPROTO_TCP;
        static constexpr DWORD dwFlags = WSA_FLAG_OVERLAPPED | WSA_FLAG_NO_HANDLE_INHERIT;
        static constexpr bool is_async = true;
        using native_sockaddr = sockaddr_in;
        using native_address = IN_ADDR;
        using native_socket = SOCKET;
        using endpoint = ip4_tcp_endpoint;
    };

    struct inheritable_async_ip4_tcp_specs
    {
        static constexpr int af = AF_INET;
        static constexpr int type = SOCK_STREAM;
        static constexpr int protocol = IPPROTO_TCP;
        static constexpr DWORD dwFlags = WSA_FLAG_OVERLAPPED;
        static constexpr bool is_async = true;
        using native_sockaddr = sockaddr_in;
        using native_address = IN_ADDR;
        using native_socket = SOCKET;
        using endpoint = ip4_tcp_endpoint;
    };

//    template <>
//    struct native_socket_specs< async_ip6_tcp_specs >
//    {
//        static constexpr int af = AF_INET6;
//        static constexpr int type = SOCK_STREAM;
//        static constexpr int protocol = IPPROTO_TCP;
//        static constexpr DWORD dwFlags = WSA_FLAG_OVERLAPPED | WSA_FLAG_NO_HANDLE_INHERIT;
//        using native_sockaddr = sockaddr_in6;
//        using native_address = IN6_ADDR;
//        using native_socket = SOCKET;
//        //using ref_type = async_ip6_tcp_acceptor_ref;
//        using endpoint = ip6_tcp_endpoint;
//    };
//
//    template <>
//    struct native_socket_specs< async_ip4_tcp_connection >
//    {
//        static constexpr int af = AF_INET;
//        static constexpr int type = SOCK_STREAM;
//        static constexpr int protocol = IPPROTO_TCP;
//        static constexpr DWORD dwFlags = WSA_FLAG_OVERLAPPED | WSA_FLAG_NO_HANDLE_INHERIT;
//        using native_sockaddr = sockaddr_in;
//        using native_address = IN_ADDR;
//    };
//
//    template <>
//    struct native_socket_specs< ip6_tcp_connection >
//    {
//        static constexpr int af = AF_INET6;
//        static constexpr int type = SOCK_STREAM;
//        static constexpr int protocol = IPPROTO_TCP;
//        static constexpr DWORD dwFlags = WSA_FLAG_NO_HANDLE_INHERIT;
//        using native_sockaddr = sockaddr_in6;
//    };
} // namespace rpnx::experimental

#endif
