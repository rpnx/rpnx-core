//
// Created by rnicholl on 5/1/2022.
//

#ifndef RPNXCORE_NET_TRANSFER_BUFFER_HPP
#define RPNXCORE_NET_TRANSFER_BUFFER_HPP
#include <utility>
#include <WinSock2.h>

namespace rpnx::experimental
{

    template <size_t N>
    class net_transfer_array
    {
        WSABUF wsabuf;
        std::array<std::byte, N> buf;

      public:
        net_transfer_array()
        {
            wsabuf.len = N;
            wsabuf.buf = (char *) buf.data();
        }

        std::byte * data() { return buf.data(); }
        WSABUF * native_ref() { return &wsabuf; }

        void reset()
        {
            wsabuf.len = N;
            wsabuf.buf = (char*) buf.data();
        }

        std::size_t size()
        {
            return wsabuf.len;
        }

        void reslice(std::size_t startpos, std::size_t endpos)
        {
            wsabuf.buf = (char*)(buf.data()+ startpos);
            wsabuf.len = endpos - startpos;
        }

        std::size_t get_startpos()
        {
            return wsabuf.buf - (char*)buf.data();
        }

        std::size_t get_endpos()
        {
            return get_startpos() + wsabuf.len;
        }


        std::byte & operator[](std::size_t n)
        {
            return (std::byte&) wsabuf.buf[n];
        }

        std::byte const & operator[](std::size_t n) const
        {
            return (std::byte const &) wsabuf.buf[n];
        }
    };
    /** A class that provides a transfer buffer that can be used to efficiently shuffle data to and from the network.6
     *
     */
    class net_transfer_buffer
    {
      public:
        using value_type = std::byte;
    };
}

#endif // RPNXCORE_NET_TRANSFER_BUFFER_HPP
