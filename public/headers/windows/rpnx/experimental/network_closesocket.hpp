//
// Created by rnicholl on 5/1/2022.
//

#ifndef RPNXCORE_NETWORK_CLOSESOCKET_HPP
#define RPNXCORE_NETWORK_CLOSESOCKET_HPP

#include "rpnx/experimental/networking_core_types_basic.hpp"

#include <WinSock2.h>

namespace rpnx::experimental
{
    inline auto native_close_socket(native_socket_type skt)
    {
        return ::closesocket(skt);
    }
}

#endif // RPNXCORE_NETWORK_CLOSESOCKET_HPP
