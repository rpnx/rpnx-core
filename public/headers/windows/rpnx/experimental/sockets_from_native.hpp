//
// Created by rnicholl on 4/24/2022.
//

#ifndef RPNXCORE_SOCKETS_FROM_NATIVE_HPP
#define RPNXCORE_SOCKETS_FROM_NATIVE_HPP
#include "rpnx/experimental/networking_core_types_fwd.hpp"
#include "rpnx/experimental/network_native_socket_types.hpp"
#include "rpnx/experimental/network_os_error_codes.hpp"
#include "rpnx/experimental/network_closesocket.hpp"
#include <syncstream>

namespace rpnx::experimental
{
    template <typename Specs >
    struct net_tcp_implementation
    {
        class acceptor_ref;
        class acceptor;

        class acceptor_ref
        {
            typename Specs::native_socket m_socket;

          public:
            acceptor_ref() : m_socket(native_invalid_socket_value)
            {
            }
            acceptor_ref(acceptor_ref const&) = default;
            explicit acceptor_ref(native_socket_type value) : m_socket(value)
            {
            }

            [[nodiscard]] typename Specs::native_socket native() const noexcept
            {
                return m_socket;
            }

            inline bool operator ==(acceptor_ref const & other) const noexcept { return m_socket == other.m_socket;  }
            inline bool operator !=(acceptor_ref const & other) const noexcept { return m_socket != other.m_socket;  }
            inline bool operator <=(acceptor_ref const & other) const noexcept { return m_socket <= other.m_socket;  }
            inline bool operator >=(acceptor_ref const & other) const noexcept { return m_socket >= other.m_socket;  }
            inline bool operator <(acceptor_ref const & other) const noexcept { return m_socket < other.m_socket;  }
            inline bool operator >(acceptor_ref const & other) const noexcept { return m_socket > other.m_socket;  }

        };

        class connection_ref
        {
            typename  Specs::native_socket m_socket;

          public:
            connection_ref() : m_socket(native_invalid_socket_value)
            {
            }
            connection_ref(connection_ref const&) = default;
            explicit connection_ref(native_socket_type value) : m_socket(value)
            {
            }

            [[nodiscard]] typename Specs::native_socket native() const noexcept
            {
                return m_socket;
            }

            inline bool operator ==(connection_ref const & other) const noexcept { return m_socket == other.m_socket;  }
            inline bool operator !=(connection_ref const & other) const noexcept { return m_socket != other.m_socket;  }
            inline bool operator <=(connection_ref const & other) const noexcept { return m_socket <= other.m_socket;  }
            inline bool operator >=(connection_ref const & other) const noexcept { return m_socket >= other.m_socket;  }
            inline bool operator <(connection_ref const & other) const noexcept { return m_socket < other.m_socket;  }
            inline bool operator >(connection_ref const & other) const noexcept { return m_socket > other.m_socket;  }

        };
        class acceptor
        {
          private:
            acceptor_ref m_socket;
          public:
            acceptor() = delete;
            acceptor(typename  Specs::endpoint ep)
            {
                m_socket = acceptor_ref( WSASocketW(  Specs ::af,Specs::type,  Specs::protocol,
                                                      nullptr, 0, Specs::dwFlags));

                if (m_socket.native() == native_invalid_socket_value)
                {
                    throw network_error("invalid socket", get_os_network_error_code());
                }

                auto endpoint_native = ep.to_sockaddr_native();

                auto bind_result = ::bind(m_socket.native(),
                                                           (sockaddr*) &endpoint_native,
                                                           sizeof(endpoint_native));

                if (bind_result != 0)
                {
                    closesocket(m_socket.native());
                    throw network_error(__FUNCTION__, get_os_network_error_code());
                }

                [[maybe_unused]] auto listen_result = ::listen(m_socket.native(), SOMAXCONN);

                if (listen_result != 0)
                {
                    closesocket(m_socket.native());
                    throw network_error(__FUNCTION__, get_os_network_error_code());
                }

            }

            operator acceptor_ref() const
            {
                return m_socket;
            }

        };
        class connection
        {
          private:
            connection_ref m_socket;

          public:

            explicit inline connection(native_socket_type socket, adopt_resource_t)
                :m_socket(socket) {
                std::osyncstream (std::cerr) << "Socket " << m_socket.native() << " RAII established" << std::endl;
            }
            inline connection(connection_ref socket, adopt_resource_t)
                :m_socket(socket) {
                std::osyncstream (std::cerr) << "Socket " << m_socket.native() << " RAII established" << std::endl;
            }

            connection() = delete;

            connection(connection const &) = delete;

            connection(connection && other) noexcept
                :m_socket()
            {
                std::swap(m_socket, other.m_socket);
            }

            ~connection()
            {

                if (m_socket != connection_ref())
                {
                    std::osyncstream (std::cerr) << "Socket " << m_socket.native() << " closed" << std::endl;
                    native_close_socket(m_socket.native());
                }
            }

            native_socket_type native() const noexcept
            {
                return m_socket.native();
            }

            native_socket_type extract_native() noexcept
            {
                auto sock = m_socket;
                m_socket = connection_ref();
                return sock.native();
            }

            operator connection_ref() const noexcept
            {
                return m_socket;
            }


        };

    };

    using async_ip6_tcp_acceptor = typename net_tcp_implementation< async_ip6_tcp_specs >::acceptor;
    using async_ip6_tcp_acceptor_ref = typename net_tcp_implementation< async_ip6_tcp_specs >::acceptor_ref;
    using async_ip6_tcp_connection = typename net_tcp_implementation< async_ip6_tcp_specs >::connection;
    using async_ip6_tcp_connection_ref = typename net_tcp_implementation< async_ip6_tcp_specs >::connection_ref;

    using async_ip4_tcp_acceptor = typename net_tcp_implementation< async_ip4_tcp_specs >::acceptor;
    using async_ip4_tcp_acceptor_ref = typename net_tcp_implementation< async_ip4_tcp_specs >::acceptor_ref;
    using async_ip4_tcp_connection = typename net_tcp_implementation< async_ip4_tcp_specs >::connection;
    using async_ip4_tcp_connection_ref = typename net_tcp_implementation< async_ip4_tcp_specs >::connection_ref;

    using inheritable_async_ip6_tcp_acceptor = typename net_tcp_implementation< inheritable_async_ip6_tcp_specs >::acceptor;
    using inheritable_async_ip6_tcp_acceptor_ref = typename net_tcp_implementation< inheritable_async_ip6_tcp_specs >::acceptor_ref;
    using inheritable_async_ip6_tcp_connection = typename net_tcp_implementation< inheritable_async_ip6_tcp_specs >::connection;
    using inheritable_async_ip6_tcp_connection_ref = typename net_tcp_implementation< inheritable_async_ip6_tcp_specs >::connection_ref;

    using inheritable_async_ip4_tcp_acceptor = typename net_tcp_implementation< inheritable_async_ip4_tcp_specs >::acceptor;
    using inheritable_async_ip4_tcp_acceptor_ref = typename net_tcp_implementation< inheritable_async_ip4_tcp_specs >::acceptor_ref;
    using inheritable_async_ip4_tcp_connection = typename net_tcp_implementation< inheritable_async_ip4_tcp_specs >::connection;
    using inheritable_async_ip4_tcp_connection_ref = typename net_tcp_implementation< inheritable_async_ip4_tcp_specs >::connection_ref;


}

#endif // RPNXCORE_SOCKETS_FROM_NATIVE_HPP
