# rpnx-core

A generic programming C++ library that provides cross platform C++ OS abstractions, data structures, and algorithms.

This library is a work in progress, some things work, other things don't. Some things are implemented only on certain OS.

If you use this library, all symbols in the rpnx:: namespace and macros that begin with RPNX_ are reserved for use by the library.

Documentation will (eventually) be available here: https://rpnx-net.github.io/rpnx-core-docs-prebuilt/annotated.html

Documentation source (doxygen): https://github.com/rpnx-net/rpnx-core-docs

Supported OS:

* Windows
* MacOS
* Linux
* Android
* FreeBSD

Not currently supported OS:

* Solaris
* iOS
* iPadOS
* VxWorks
* Other BSD
* Illumos

Supported compilers:

* G++
* Clang++
* Microsoft Visual C++

Not currently supported compilers:

* Emscripten
* MinGW
* MinGW-w64
* ICC

Unsupported compilers and OS will be supported when possible, so long as they support recent C++ versions. If you want to use one file an issue and I will take a look at it.
